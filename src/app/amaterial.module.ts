import { MatButtonModule, MatCheckboxModule } from '@angular/material';
import { NgModule } from '@angular/core';

import { MatToolbarModule } from '@angular/material/toolbar';
import { MatIconModule } from '@angular/material/icon';
import { MatMenuModule } from '@angular/material/menu';
import { MatCardModule } from '@angular/material/card';
import { MatGridListModule } from '@angular/material/grid-list';
import { MatExpansionModule } from '@angular/material/expansion';
import { MatChipsModule } from '@angular/material/chips';
import { MatDialogModule } from '@angular/material/dialog';
import { MatSidenavModule } from '@angular/material/sidenav';
import { MatListModule} from '@angular/material/list';
import { MatFormFieldModule } from '@angular/material';
import { MatInputModule } from '@angular/material';
import { MatTreeModule } from '@angular/material/tree';
import { MatTabsModule } from '@angular/material/tabs';
import { MatDividerModule } from '@angular/material/divider';
import { MatSelectModule } from '@angular/material/select';
import { MatDatepickerModule } from '@angular/material/datepicker';
import { MatNativeDateModule } from '@angular/material';
import { MatRadioModule } from '@angular/material/radio';
import { MatTableModule } from '@angular/material/table';
import { MatSortModule } from '@angular/material/sort';
import { MatSnackBarModule } from '@angular/material/snack-bar';
import { MatProgressBarModule } from '@angular/material/progress-bar';
import { MatBadgeModule } from '@angular/material/badge';
import { MatPaginatorModule } from '@angular/material/paginator';
import { MatTooltipModule } from '@angular/material/tooltip';
import { NgxUploaderModule } from 'ngx-uploader';
@NgModule({
  imports: [MatTooltipModule, NgxUploaderModule, MatPaginatorModule, MatBadgeModule, MatProgressBarModule, MatSnackBarModule, MatSortModule, MatTableModule, MatRadioModule, MatNativeDateModule, MatDatepickerModule, MatSelectModule, MatFormFieldModule, MatDividerModule, MatTabsModule, MatTreeModule, MatInputModule, MatListModule, MatSidenavModule, MatGridListModule, MatExpansionModule, MatChipsModule, MatDialogModule, MatButtonModule, MatCheckboxModule, MatToolbarModule, MatIconModule, MatMenuModule, MatCardModule],
  providers: [MatDatepickerModule],
  exports: [MatTooltipModule, NgxUploaderModule, MatPaginatorModule, MatBadgeModule, MatProgressBarModule, MatSnackBarModule, MatSortModule, MatTableModule, MatRadioModule, MatNativeDateModule, MatDatepickerModule, MatSelectModule, MatFormFieldModule, MatDividerModule, MatTabsModule, MatTreeModule, MatInputModule, MatListModule, MatSidenavModule, MatGridListModule, MatExpansionModule, MatChipsModule, MatDialogModule, MatButtonModule, MatCheckboxModule, MatToolbarModule, MatIconModule, MatMenuModule, MatCardModule],
})

export class MaterialModule {
    
}