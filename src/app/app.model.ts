import * as SELECTOR from './selector.enums';
import * as moment from 'moment';

export class Usuario {
    id: number;
    documento: SELECTOR.UsuarioDocumento;
    cedula: number;
    avatar: string;
    nombres: string;
    apellidos: string;
    nacimiento: string;
    direccion: string;
    telefono: string;
    correo_personal: string;
    correo_academico: string;
    cargo: string;
    descripcion: string;
    correo_principal: string;
    tipo: SELECTOR.UsuarioTipo;
    estado: SELECTOR.UsuarioEstado;
    solicitudes: Array<Solicitud>;
    usuarioBloqueado: UsuarioBloqueado;
    solicitudInterasados: Array<SolicitudInteresado>;
    notificaciones: Array<Notificacion>;
    created_at: string;
    updated_at: string;
}

export class UsuarioBloqueado {
    id: number;
    usuario: Usuario = new Usuario();
    motivo: string;
    created_at: string;
    updated_at: string;
}

export class Notificacion {
    id: number;
    usuario: Usuario = new Usuario();
    descripcion: string;
    read_at: string;
    created_at: string;
    updated_at: string;
}

export class PasswordRess {
    id: number;
    email: string;
    token: string;
    created_at: string;
    updated_at: string;
}

export class Material {
    id: number;
    nombre: string;
    descripcion: string;
    cantidadExistencia: number;
    cantidadMinimo: number;
    created_at: string;
    updated_at: string;
}

export class Solicitud {
    id: number;
    solicitante_id: number;
    solicitante: Usuario;
    solicitante_documento: SELECTOR.UsuarioDocumento;
    solicitante_cedula: number;
    solicitante_nombres: string;
    solicitante_apellidos: string;
    solicitante_telefono: string;
    solicitante_correo: string;
    solicitante_cargo: string;
    acceso: SELECTOR.SolicitudAcceso;
    categoria: SELECTOR.SolicitudCategoria;
    asunto: string;
    descripcion: string;
    solicitud_tipo: SELECTOR.SolicitudTipo;
    solicitud_fecha: string;
    diagnostico_descripcion: string;
    estado: SELECTOR.SolicitudEstado;
    tiempo_pendiente: number;
    tiempo_aprobado: number;
    tiempo_rechazado: number;
    tiempo_resuelto: number;
    tiempo_inconcluso: number;
    prioridad: number;
    solicitudInteresados: Array<SolicitudInteresado>;
    interesados: Array<Usuario>;
    solicitudMateriales: Array<SolicitudMaterial>;
    solicitudSis: SolicitudSis;
    solicitudSisse: SolicitudSisse;
    created_at: string;
    updated_at: string;
    deleted_at: string;
}

export class SolicitudSis {
    id: number;
    solicitudId: Solicitud = new Solicitud();
    fotoPrevia: string;
    lugar: string;
    diagnosticoResponsable: string;
    diagnosticoFoto: string;
    created_at: string;
    updated_at: string;
}

export class SolicitudSisse {
    id: number;
    solicitudId: Solicitud = new Solicitud();
    fechaSalida: string;
    fechaRegreso: string;
    lugarSalida: string;
    lugarLlegada: string;
    cantidadPersonas: number;
    choferAsignado: string;
    created_at: string;
    updated_at: string;
}

export class SolicitudMotivo {
    id: number;
    categoria: SELECTOR.SolicitudCategoria;
    asunto: string;
    solicitudes: Array<Solicitud>;
    created_at: string;
    updated_at: string;
}

export class SolicitudMaterial {
    id: number;
    solicitud: Solicitud = new Solicitud();
    materialNombre: string;
    materialCantidad: number;
    created_at: string;
    updated_at: string;
}

export class SolicitudInteresado {
    id: number;
    usuario_id: number;
    solicitud_id: number;
    avatar: string;
    nombres: string;
    apellidos: string;
    created_at: string;
    updated_at: string;
}

export const UsuarioDocumentos: any = [
    {nombre: 'Venezolano', valor: 'v'}, 
    {nombre: 'Extranjero', valor: 'e'}, 
    // {nombre: 'Rif', valor: 'j'}
];
export const UsuarioTipos: any = [
    {nombre: 'Personal UDO', valor: 'personal'}, 
    {nombre: 'Administrador', valor: 'administrador'}
];
export const UsuarioEstados: any = [
    {nombre: 'Pendiente', valor: 'pendiente'}, 
    {nombre: 'Activo', valor: 'activo'}, 
    {nombre: 'Bloqueado', valor: 'bloqueado'}
];

export const SolicitudAccesos: any = [
    {nombre: 'Público', valor: 'publico'}, 
    {nombre: 'Privado', valor: 'privado'}
];
export const SolicitudCategorias: any = [
    {nombre: 'Aseo y limpieza', valor: 'aseo y limpieza', icon: 'nature_people'}, 
    {nombre: 'Jardinería', valor: 'jardineria', icon: 'local_florist'}, 
    {nombre: 'Plomería', valor: 'plomeria', icon: 'build'}, 
    {nombre: 'Electricidad', valor: 'electricidad', icon: 'flash_on'}, 
    {nombre: 'Refrigeración', valor: 'refrigeracion', icon: 'kitchen'}, 
    {nombre: 'Pintura', valor: 'pintura', icon: 'format_paint'}, 
    {nombre: 'Carpintería', valor: 'carpinteria', icon: 'toll'}, 
    {nombre: 'Herrería', valor: 'herreria', icon: 'settings_applications'}, 
    {nombre: 'Transporte', valor: 'transporte', icon: 'airport_shuttle'}
];
export const SolicitudTipos: any =  [
    {nombre: 'Solicitud Interna de Servicio', valor: 'sis'}, 
    {nombre: 'Solicitud Interna de Servicio de Salidas Especiales', valor: 'sisse'}
];
export const SolicitudEstados: any = [
    {nombre: 'Pendiente', valor: 'pendiente', background: '#03DAC6', icon: 'hourglass_empty'}, 
    {nombre: 'Aprobado', valor: 'aprobado', background: '#0336ff', icon: 'done'}, 
    {nombre: 'Rechazado', valor: 'rechazado', background: '#B71C1C', icon: 'info'}, 
    {nombre: 'Resuelto', valor: 'resuelto', background: '#5E35B1', icon: 'done_all'}, 
    {nombre: 'Inconcluso', valor: 'inconcluso', background: '#B00020', icon: 'block'}
];
export const SolicitudPrioridades: any = [
    {nombre: 'Uno', valor: 1}, 
    {nombre: 'Dos', valor: 2}, 
    {nombre: 'Tres', valor: 3}, 
    {nombre: 'Cuatro', valor: 4}, 
    {nombre: 'Cinco', valor: 5}
];