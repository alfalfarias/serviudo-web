import { BrowserModule } from '@angular/platform-browser';
import { NgModule, ErrorHandler } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { HTTP_INTERCEPTORS, HttpClientModule } from '@angular/common/http';

import * as moment from 'moment';

import { AppComponent } from './app.component';

//ANIMATIONS
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { NoopAnimationsModule } from '@angular/platform-browser/animations';

//ANGULAR MATERIAL
import { MaterialModule } from './amaterial.module';

//SERVICES
import { SolicitudSisService } from './services/solicitudsis.service';
import { SolicitudSisseService } from './services/solicitudsisse.service';
import { SolicitudInteresadosService } from './services/solicitud-interesados.service';
import { AuthService } from './services/auth.service';
import { XhrErrorHandlerService } from './services/xhr-error-handler.service';
import { ProgressBarService } from './services/progress-bar.service';

//RUTAS
import { AppRoutingModule } from './app.routing.module';

//INTERCEPTOR
import { AuthInterceptor } from './http-interceptor/auth-interceptor';
import { ProgressBarInterceptor } from './http-interceptor/progress-bar-interceptor';

//GUARDS
import { AccesoAdminGuard } from './guards/acceso-admin.guard';
import { AccesoPersonalGuard } from './guards/acceso-personal.guard';
import { AccesoSesionGuard } from './guards/acceso-sesion.guard';
import { AccesoPublicoGuard } from './guards/acceso-publico.guard';

//PIPES
import { DatePipe } from '@angular/common';
import { TruncatePipe } from './pipes/truncate.pipe';
import { TiempoEnHumanoPipe, ContadorTiempoPipe } from './pipes/tiempo-en-humano.pipe';
import { FormatoFechaPipe } from './pipes/formato-fecha.pipe';
import { ColorCategoriaPipe, ColorEstadoPipe } from './pipes/solicitud-color.pipe';
import { PrimeraPalabraPipe } from './pipes/primera-palabra.pipe';
import { SolicitudIconoPipe } from './pipes/solicitud-icono.pipe';

//Navegación
import { DashboardComponent } from './dashboard/dashboard.component';

//Componentes de entrada
import { EntryComponentModule } from '../entry-component/entry-component.module';

//COMPONENTES
//Auth
import { LoginComponent } from './auth/login/login.component';
import { RegistroComponent } from './auth/registro/registro.component';
import { RecuperarPasswordComponent } from './auth/recuperar-password/recuperar-password.component';
import { CambiarPasswordComponent } from './auth/cambiar-password/cambiar-password.component';

//Solicitud de Servicio
import { SimpleSisComponent } from './solicitud-interna/sis/simple-sis/simple-sis.component';
import { SimpleSisseComponent } from './solicitud-interna/sisse/simple-sisse/simple-sisse.component';

import { SolicitudInternaComponent } from './solicitud-interna/solicitud-interna.component';
import { EvaluacionSisseComponent } from './solicitud-interna/sisse/evaluacion-sisse/evaluacion-sisse.component';
import { GestionSisseComponent } from './solicitud-interna/sisse/gestion-sisse/gestion-sisse.component';
import { GestioneSisComponent } from './solicitud-interna/sis/gestione-sis/gestione-sis.component';
import { EvaluacionSisComponent } from './solicitud-interna/sis/evaluacion-sis/evaluacion-sis.component';
import { ChartModule } from 'angular-highcharts';
import { PersonalSisComponent } from './solicitud-interna/sis/personal-sis/personal-sis.component';
import { PersonalSisseComponent } from './solicitud-interna/sisse/personal-sisse/personal-sisse.component';
import { EliminarSolicitudComponent } from './solicitud-interna/modals/eliminar-solicitud/eliminar-solicitud.component';
import { EvaluacionSisListComponent } from './evaluacion-sis-list/evaluacion-sis-list.component';
import { EvaluacionSisseListComponent } from './evaluacion-sisse-list/evaluacion-sisse-list.component';
import { PersonalSisTableComponent } from './personal-sis-table/personal-sis-table.component';
import { PersonalSisseTableComponent } from './personal-sisse-table/personal-sisse-table.component';
import { RegistroUsuarioComponent } from './usuarios/registro-usuario/registro-usuario.component';
import { RegistroUsuarioListComponent } from './registro-usuario-list/registro-usuario-list.component';
import { NotificacionComponent } from './notificacion/notificacion.component';
import { ConfiguracionCuentaComponent } from './usuarios/configuracion-cuenta/configuracion-cuenta.component';
import { MaterialesTableComponent } from './materiales-table/materiales-table.component';

@NgModule({
    declarations: [
        AppComponent,
        LoginComponent,
        RegistroComponent,
        RecuperarPasswordComponent,
        CambiarPasswordComponent,
        SolicitudInternaComponent,
        SimpleSisComponent,
        SimpleSisseComponent,
        EvaluacionSisseComponent,
        GestionSisseComponent,
        GestioneSisComponent,
        EvaluacionSisComponent,
        PersonalSisComponent,
        PersonalSisseComponent,
        EliminarSolicitudComponent,
        EvaluacionSisListComponent,
        EvaluacionSisseListComponent,
        PersonalSisTableComponent,
        PersonalSisseTableComponent,
        RegistroUsuarioComponent,
        RegistroUsuarioListComponent,
        NotificacionComponent,
        ConfiguracionCuentaComponent,
        MaterialesTableComponent,
        DashboardComponent,
        TruncatePipe,
        TiempoEnHumanoPipe,
        FormatoFechaPipe,
        ColorCategoriaPipe,
        ColorEstadoPipe,
        PrimeraPalabraPipe,
        ContadorTiempoPipe,
        SolicitudIconoPipe,
    ],

    entryComponents: [],

    imports: [
        BrowserModule,
        BrowserAnimationsModule,
        MaterialModule,
        AppRoutingModule,
        EntryComponentModule,
        HttpClientModule,
        FormsModule, 
        ReactiveFormsModule,
        ChartModule,
    ],

    providers: [
        SolicitudSisService, 
        SolicitudSisseService, 
        AuthService, 
        DatePipe,
        ProgressBarService,
        SolicitudInteresadosService,
        XhrErrorHandlerService,
        { provide: ErrorHandler, useClass: XhrErrorHandlerService },
        { provide: HTTP_INTERCEPTORS, useClass: AuthInterceptor, multi: true },
        { provide: HTTP_INTERCEPTORS, useClass: ProgressBarInterceptor, multi: true },
        AccesoAdminGuard,
        AccesoPersonalGuard,
        AccesoPublicoGuard,
        AccesoSesionGuard,
    ],
    bootstrap: [AppComponent]
})
export class AppModule { }