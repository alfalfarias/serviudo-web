import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

//GUARDS
import { AccesoAdminGuard } from './guards/acceso-admin.guard';
import { AccesoPersonalGuard } from './guards/acceso-personal.guard';
import { AccesoSesionGuard } from './guards/acceso-sesion.guard';
import { AccesoPublicoGuard } from './guards/acceso-publico.guard';

import { SolicitudInternaComponent } from './solicitud-interna/solicitud-interna.component';
import { LoginComponent } from './auth/login/login.component';
import { RegistroComponent } from './auth/registro/registro.component';
import { RecuperarPasswordComponent } from './auth/recuperar-password/recuperar-password.component';
import { CambiarPasswordComponent } from './auth/cambiar-password/cambiar-password.component';
import { EvaluacionSisListComponent } from './evaluacion-sis-list/evaluacion-sis-list.component';
import { EvaluacionSisseListComponent } from './evaluacion-sisse-list/evaluacion-sisse-list.component';
import { PersonalSisTableComponent } from './personal-sis-table/personal-sis-table.component';
import { PersonalSisseTableComponent } from './personal-sisse-table/personal-sisse-table.component';
import { RegistroUsuarioListComponent } from './registro-usuario-list/registro-usuario-list.component';
import { NotificacionComponent } from './notificacion/notificacion.component';
import { ConfiguracionCuentaComponent } from './usuarios/configuracion-cuenta/configuracion-cuenta.component';
import { DashboardComponent } from './dashboard/dashboard.component';
import { MaterialesTableComponent } from './materiales-table/materiales-table.component';

const routes: Routes = [
  { path: '', redirectTo: 'auth/login', pathMatch: 'full' },
  {
    path: '',
    component: DashboardComponent,
    children: [
      {
        path: 'solicitudes/:type',
        component: SolicitudInternaComponent
      },
      {
        path: 'auth/login',
        component: LoginComponent,
        canActivate: [AccesoPublicoGuard]
      },
      {
        path: 'auth/registro/personal-udo',
        component: RegistroComponent,
        canActivate: [AccesoPublicoGuard]
      },
      {
        path: 'auth/recuperar-password',
        component: RecuperarPasswordComponent,
        canActivate: [AccesoPublicoGuard]
      },
      {
        path: 'auth/cambiar-password',
        component: CambiarPasswordComponent,
        canActivate: [AccesoPublicoGuard]
      },
      {
        path: 'personal/solicitudes/sis',
        component: PersonalSisTableComponent,
        canActivate: [AccesoSesionGuard]
      },
      {
        path: 'personal/solicitudes/sisse',
        component: PersonalSisseTableComponent,
        canActivate: [AccesoSesionGuard]
      },
      {
        path: 'notificaciones',
        component: NotificacionComponent,
        canActivate: [AccesoSesionGuard]
      },
      {
        path: 'configuracion-cuenta',
        component: ConfiguracionCuentaComponent,
        canActivate: [AccesoSesionGuard]
      },
      {
        path: 'personal/solicitudes/sis',
        component: PersonalSisTableComponent,
        canActivate: [AccesoPublicoGuard]
      },
      {
        path: 'personal/solicitudes/sisse',
        component: PersonalSisseTableComponent,
        canActivate: [AccesoPublicoGuard]
      },
      {
        path: 'evaluar/solicitudes/sis',
        component: EvaluacionSisListComponent,
        canActivate: [AccesoAdminGuard]
      },
      {
        path: 'evaluar/solicitudes/sisse',
        component: EvaluacionSisseListComponent,
        canActivate: [AccesoAdminGuard]
      },
      {
        path: 'evaluar/solicitud-registro-usuario',
        component: RegistroUsuarioListComponent,
        canActivate: [AccesoAdminGuard]
      },
      {
        path: 'materiales',
        component: MaterialesTableComponent,
        canActivate: [AccesoAdminGuard]
      }
    ]
  },
  // { path: '**', redirectTo: 'solicitudes/sis' }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
