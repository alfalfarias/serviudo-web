import { Component, OnInit, OnDestroy } from '@angular/core';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { Router, ActivatedRoute } from '@angular/router';
import { AuthService } from '../../services/auth.service';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';

import { Subscription } from 'rxjs/Subscription';
import { UsuarioTipo, UsuarioEstado } from '../../selector.enums';

@Component({
    selector: 'app-login',
    templateUrl: './login.component.html',
    styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit, OnDestroy { 

    private subscriptions = new Array<Subscription>();
    public progressBar: string;

    formulario = new FormGroup({
        correo_principal: new FormControl ('personal@example.com', [ Validators.required, Validators.email]),
        password: new FormControl ('123456', [ Validators.required, Validators.minLength(6) ])
    });

    is_hidde_password = true;

    constructor(
        private router: Router,
        private route: ActivatedRoute,
        private authService: AuthService
        ) { }

    ngOnInit() {
        //
    }
    ngOnDestroy(){
        if (this.subscriptions) {
            this.subscriptions.forEach(subscription => {
                subscription.unsubscribe();
            });
        }
    }
    public login(){
        this.subscriptions.push(this.authService.login(this.formulario.value)
            .subscribe(
                response => {
                    if (response.token !== null) {
                        return this.authService.authenticate()
                        .subscribe(
                            response => {
                                this.router.navigate(['/solicitudes', 'sis']);
                                return response;
                            }
                        );
                    }
                }
            )
        );
    }
}