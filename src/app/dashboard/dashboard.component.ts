import { Component, OnInit, AfterViewInit, OnDestroy } from '@angular/core';
import { Subscription } from 'rxjs/Subscription';
import { Router, ActivatedRoute } from '@angular/router';

import { AuthService } from '../services/auth.service';
import { ProgressBarService, ProgressBar as ProgressBarInterface } from '../services/progress-bar.service';

import { Usuario } from '../app.model';
import { MatSnackBar } from '@angular/material';

@Component({
    selector: 'app-dashboard',
    templateUrl: './dashboard.component.html',
    styleUrls: ['./dashboard.component.scss']
})
export class DashboardComponent implements OnInit, AfterViewInit, OnDestroy {
    private subscriptions = new Array<Subscription>();
    public usuario: Usuario = new Usuario();
    public progressBar: ProgressBarInterface;
    public sidenav: any = {
        show: false
    };
    public dashboard: any = {
        administrador: [
        {
            nombre: 'Mis solicitudes SIS',
            ruta: '/personal/solicitudes/sis'
        },
        {
            nombre: 'Mis solicitudes SISSE',
            ruta: '/personal/solicitudes/sisse'
        },
        {
            nombre: 'Lista general de SIS',
            ruta: '/solicitudes/sis'
        },
        {
            nombre: 'Lista general de SISSE',
            ruta: '/solicitudes/sisse'
        },
        {
            nombre: 'Evaluacion de SIS',
            ruta: '/evaluar/solicitudes/sis'
        },
        {
            nombre: 'Evaluacion de SISSE',
            ruta: '/evaluar/solicitudes/sisse'
        },
        {
            nombre: 'Conteo estadístico',
            ruta: ''
        },
        {
            nombre: 'Gestión de Almacén',
            ruta: '/materiales'
        },
        {
            nombre: 'Gestión de solicitudes de registro de usuarios',
            ruta: ''
        },
        {
            nombre: 'Gestión de usuarios bloqueados',
            ruta: ''
        },
        {
            nombre: 'Gestión de usuarios Personal',
            ruta: ''
        },
        {
            nombre: 'Gestión de usuarios Administrador',
            ruta: ''
        }
        ],
        personal: [
        {
            nombre: 'Mis solicitudes SIS',
            ruta: '/personal/solicitudes/sis'
        },
        {
            nombre: 'Mis solicitudes SISSE',
            ruta: '/personal/solicitudes/sisse'
        },
        {
            nombre: 'Lista general de SIS',
            ruta: '/solicitudes/sis'
        },
        {
            nombre: 'Lista general de SISSE',
            ruta: '/solicitudes/sisse'
        },
        {
            nombre: 'Conteo estadístico',
            ruta: ''
        }
        ],
    };

    constructor(
        private router: Router,
        private route: ActivatedRoute,
        private authService: AuthService,
        private progressBarService: ProgressBarService,
        private snackBar: MatSnackBar
        ) { 
    }

    ngOnInit() {
        this.progressBar = <ProgressBarInterface>{ show: false, mode: 'determinate' };
        this.subscriptions.push(
            this.authService.getUsuario().subscribe((usuario) => {
                this.usuario = usuario;
            })
            );
        this.subscriptions.push(
            this.authService.getUsuario()
            .subscribe(usuario => {
                this.usuario = usuario;
                if (usuario.tipo != null) {
                    this.sidenav.show = true;
                }
                else{
                    this.sidenav.show = false;
                }
            })
            );
    }
    ngAfterViewInit() {
        this.subscriptions.push(
            this.progressBarService.getMode()
            .subscribe(progressBar => { 
                this.progressBar = progressBar; 
            })
            );
    }
    ngOnDestroy(){
        if (this.subscriptions) {
            this.subscriptions.forEach(subscription => {
                subscription.unsubscribe();
            });
        }
    }
    cerrarSesion(){
        this.authService.logout();
        this.router.navigate(['/auth/login']);
        this.snackBar.open('Se ha cerrado la sesión', '200 OK', {
            duration: 4000,
        });
    }
    redirectTest(){
        this.router.navigate(['/solicitudes/sis']);
    }
}
