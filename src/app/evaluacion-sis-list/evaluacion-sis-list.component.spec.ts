import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { EvaluacionSisListComponent } from './evaluacion-sis-list.component';

describe('EvaluacionSisListComponent', () => {
  let component: EvaluacionSisListComponent;
  let fixture: ComponentFixture<EvaluacionSisListComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ EvaluacionSisListComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(EvaluacionSisListComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
