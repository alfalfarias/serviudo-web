import { Component, OnInit, AfterViewInit, OnDestroy } from '@angular/core';
import { FormGroup, FormControl } from '@angular/forms';
import { MatDialog, PageEvent } from '@angular/material';

import { Subscription } from 'rxjs/Subscription';
// import { RegistroSisComponent } from './sis/modals/registro-sis/registro-sis.component';
// import { RegistroSisseComponent } from './sisse/modals/registro-sisse/registro-sisse.component';
import { SolicitudSisService } from '../services/solicitudsis.service';
import { Solicitud } from '../app.model';
import Swal from 'sweetalert';

@Component({
  selector: 'app-evaluacion-sis-list',
  templateUrl: './evaluacion-sis-list.component.html',
  styleUrls: ['../solicitud-interna/solicitud-interna.component.scss']
})
export class EvaluacionSisListComponent implements OnInit, AfterViewInit, OnDestroy {

  private subscriptions: Array<Subscription> = new Array<Subscription>();
  solicitudes: Array<Solicitud>;
  solicitudTipo: string = 'sis';
  busqueda: FormGroup = new FormGroup({
    filtro: new FormControl ('asunto'),
    valor: new FormControl ('')
  });

    public dataSource: Array<Solicitud> = new Array<Solicitud>();

    pagination: any = {
      length: 10,
      pageSize: 10,
      pageIndex: 0,
      pageSizeOptions: [5, 10, 25, 100]
    };
    pageEvent: PageEvent = new PageEvent();

    constructor(private solicitudSisService: SolicitudSisService,
      public dialog: MatDialog) { 
    }

  ngOnInit() {

    this.subscriptions.push(
      this.solicitudSisService.index()
      .subscribe(solicitudes => {
        this.solicitudes = solicitudes.sort((a, b) => b.id - a.id);
        this.dataSource = this.solicitudes.slice(0, this.pagination.pageSize);

        this.pagination.length = this.solicitudes.length;

        this.pageEvent.length = this.solicitudes.length;
        this.pageEvent.pageSize = this.pagination.pageSize;
        this.pageEvent.pageIndex = this.pagination.pageIndex;
        this.refrescarPaginacion(this.pageEvent);
      })
      );
      this.solicitudes = this.solicitudSisService.solicitudesSis;
  }

  ngAfterViewInit() {
    const interval = 1000;
    setInterval(() => {
      for (var i = 0; i >= this.dataSource.length; i++) {
        this.dataSource[i].tiempo_aprobado += interval;
      }
    }, interval);
  }

  ngOnDestroy(){
    if (this.subscriptions) {
      this.subscriptions.forEach(subscription => {
        subscription.unsubscribe();
      });
    }
  }
  refrescarPaginacion(pageEvent?:PageEvent){
    this.pageEvent = pageEvent;
    this.pageEvent.length = this.solicitudes.length;
    const start: number = this.pageEvent.pageIndex * this.pageEvent.pageSize;
    const end: number = start + this.pageEvent.pageSize;
    this.dataSource = this.solicitudes.slice(start, end);
  }

  applyFilter() {
    const value: string = this.busqueda.value.valor.trim().toLowerCase();

    this.subscriptions.push(
      this.solicitudSisService.index()
      .subscribe(solicitudes => {
        this.solicitudes = solicitudes.sort((a, b) => b.id - a.id);

        var filtros = ['id', 'asunto', 'categoria', 'estado', 'descripcion', 'solicitante_nombres', 'solicitante_apellidos', 'solicitante_cedula'];

        this.solicitudes = this.solicitudes.filter(solicitud  => {
          if ( solicitud.solicitud_tipo === this.solicitudTipo && (
            solicitud.id.toString().toLowerCase().includes(value) ||
            solicitud.asunto.toLowerCase().includes(value) || 
            solicitud.categoria.toLowerCase().includes(value) || 
            solicitud.estado.toLowerCase().includes(value) || 
            solicitud.descripcion.toLowerCase().includes(value) || 
            (solicitud.solicitante_nombres + ' ' + solicitud.solicitante_apellidos).toString().toLowerCase().includes(value))
            ){
            return true;
        }
        return false;
      });

        this.pagination.length = this.solicitudes.length;

        this.refrescarPaginacion(this.pageEvent);
      })
      );
  }
}
