import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { EvaluacionSisseListComponent } from './evaluacion-sisse-list.component';

describe('EvaluacionSisseListComponent', () => {
  let component: EvaluacionSisseListComponent;
  let fixture: ComponentFixture<EvaluacionSisseListComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ EvaluacionSisseListComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(EvaluacionSisseListComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
