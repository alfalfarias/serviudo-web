import { Injectable } from '@angular/core';
import { CanActivate, ActivatedRouteSnapshot, RouterStateSnapshot, Router } from '@angular/router';
import { Observable } from 'rxjs';

import { Usuario } from '../app.model';
import { UsuarioTipo } from '../selector.enums';

import { AuthService } from '../services/auth.service';

@Injectable()
export class AccesoPersonalGuard implements CanActivate {
    private usuario: Usuario = new Usuario();
    constructor(
        private authService: AuthService,
        private router: Router,
        ){}
    
    canActivate(
        next: ActivatedRouteSnapshot,
        state: RouterStateSnapshot): Observable<boolean> | Promise<boolean> | boolean {

        return this.authService.authenticate().toPromise()
        .then(usuario => {
            if (usuario.tipo === null) {
                this.router.navigate(['/auth/login']);
                return false;
            }
            if (usuario.tipo !== 'personal') {
                this.router.navigate(['/solicitudes/sis']);
                return false;
            }
            return true;
        })
        .catch(err => {
            this.router.navigate(['/auth/login']);
            return false;
        });
    }
}


