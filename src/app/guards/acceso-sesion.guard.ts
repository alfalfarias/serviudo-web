import { Injectable } from '@angular/core';
import { CanActivate, ActivatedRouteSnapshot, RouterStateSnapshot, Router } from '@angular/router';
import { Observable } from 'rxjs';

import { Usuario } from '../app.model';
import { UsuarioTipo } from '../selector.enums';

import { AuthService } from '../services/auth.service';

@Injectable()
export class AccesoSesionGuard implements CanActivate {
    private usuario: Usuario = new Usuario();
    constructor(
        private authService: AuthService,
        private router: Router,
        ){}

    canActivate(
        next: ActivatedRouteSnapshot,
        state: RouterStateSnapshot): Observable<boolean> | Promise<boolean> | boolean {
        if (this.authService.getAuthorizationToken() === null) {
            this.router.navigate(['/auth/login']);
            return false;
        }
        return true;
    }
}
