import { Injectable, Injector } from '@angular/core';
import { HttpEvent, HttpInterceptor, HttpHandler, HttpRequest, HttpResponse } from '@angular/common/http';
import { Observable, pipe } from 'rxjs';
import { tap } from 'rxjs/operators';
import { ProgressBarService } from '../services/progress-bar.service';

@Injectable()
export class ProgressBarInterceptor implements HttpInterceptor {
    constructor(private progressBarService: ProgressBarService) { }

    intercept(req: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
        this.progressBarService.show();
        return next.handle(req).pipe(tap((event: HttpEvent<any>) => { 
            if (event instanceof HttpResponse) {
                this.progressBarService.hide();
            }
        },
        (err: any) => {
            this.progressBarService.hide();
        }
        ));
    }
}
