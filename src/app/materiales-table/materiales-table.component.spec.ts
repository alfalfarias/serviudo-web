import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { MaterialesTableComponent } from './materiales-table.component';

describe('MaterialesTableComponent', () => {
  let component: MaterialesTableComponent;
  let fixture: ComponentFixture<MaterialesTableComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ MaterialesTableComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MaterialesTableComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
