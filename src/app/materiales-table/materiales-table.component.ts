import { Component, OnInit, OnDestroy, ViewChild } from '@angular/core';
import { FormGroup, FormControl, Validators } from '@angular/forms';

import { Subscription } from 'rxjs/Subscription';

import { MatPaginator,MatSort, MatTableDataSource, MatSnackBar } from '@angular/material';
import { MatDialog } from '@angular/material';

import { Material, Usuario } from '../app.model';
import { DetalleSisComponent } from '../solicitud-interna/sis/modals/detalle-sis/detalle-sis.component';
import { RegistroSisComponent } from '../solicitud-interna/sis/modals/registro-sis/registro-sis.component';
import { MaterialesService } from '../services/materiales.service';

@Component({
  selector: 'app-materiales-table',
  templateUrl: './materiales-table.component.html',
  styleUrls: ['./materiales-table.component.scss']
})
export class MaterialesTableComponent implements OnInit {

  private subscriptions = new Array<Subscription>();
  public usuario: Usuario = new Usuario();
  busqueda: FormGroup = new FormGroup({
    filtro: new FormControl ('asunto'),
    valor: new FormControl ('')
  });

  displayedColumns: string[] = ['ID', 'Nombre', 'Descripcion', 'Existencia', 'Minimo permitido'];
  dataSource: MatTableDataSource<Material>;

  @ViewChild(MatPaginator) paginator: MatPaginator;
  @ViewChild(MatSort) sort: MatSort;

  materiales: Array<Material>;

  constructor(
    private materialesService: MaterialesService,
    public dialog: MatDialog,
    public snackBar: MatSnackBar,
  ) { }

  ngOnInit() {
    this.subscriptions.push(
      this.materialesService.index()
      .subscribe(materiales => {
        this.materiales = materiales;
        this.dataSource = new MatTableDataSource(this.materiales);
        this.dataSource.paginator = this.paginator;
        this.dataSource.sort = this.sort;
      })
      );
  }
}
