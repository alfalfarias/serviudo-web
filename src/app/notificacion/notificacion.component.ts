import { Component, OnInit, OnDestroy } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { NotificacionesService } from '../services/notificaciones.service';
import { Subscription } from 'rxjs/Subscription';
import { Notificacion } from '../app.model';


@Component({
    selector: 'app-notificacion',
    templateUrl: './notificacion.component.html',
    styleUrls: ['./notificacion.component.scss']
})
export class NotificacionComponent implements OnInit, OnDestroy {
    private subscriptions = new Array<Subscription>();
    public notificaciones = new Array<Notificacion>();

    constructor(
        private router: Router,
        private route: ActivatedRoute,
        public notificacionesService: NotificacionesService,
        ) {
    }

    ngOnInit() {
        this.notificaciones = this.notificacionesService.notificaciones;
    }

    ngOnDestroy() {
        if (this.subscriptions) {
            this.subscriptions.forEach(subscription => {
                subscription.unsubscribe();
            });
        }
    }

}
