import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PersonalSisTableComponent } from './personal-sis-table.component';

describe('PersonalSisTableComponent', () => {
  let component: PersonalSisTableComponent;
  let fixture: ComponentFixture<PersonalSisTableComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PersonalSisTableComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PersonalSisTableComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
