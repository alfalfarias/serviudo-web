import { Component, OnInit, OnDestroy, ViewChild } from '@angular/core';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { Router, ActivatedRoute } from '@angular/router';

import { Subscription } from 'rxjs/Subscription';

import { MatPaginator,MatSort, MatTableDataSource, MatSnackBar } from '@angular/material';
import { MatDialog } from '@angular/material';

import { SolicitudSisService } from '../services/solicitudsis.service';
import { Solicitud, Usuario } from '../app.model';
import { DetalleSisComponent } from '../solicitud-interna/sis/modals/detalle-sis/detalle-sis.component';
import { RegistroSisComponent } from '../solicitud-interna/sis/modals/registro-sis/registro-sis.component';
import { AuthService } from '../services/auth.service';

@Component({
  selector: 'app-personal-sis-table',
  templateUrl: './personal-sis-table.component.html',
  styleUrls: ['./personal-sis-table.component.scss']
})
export class PersonalSisTableComponent implements OnInit, OnDestroy {

  private subscriptions = new Array<Subscription>();
  public usuario: Usuario = new Usuario();
  busqueda: FormGroup = new FormGroup({
    filtro: new FormControl ('asunto'),
    valor: new FormControl ('')
  });

  displayedColumns: string[] = ['ID', 'Fecha', 'Asunto', 'Categoria', 'Estado'];
  dataSource: MatTableDataSource<Solicitud>;

  @ViewChild(MatPaginator) paginator: MatPaginator;
  @ViewChild(MatSort) sort: MatSort;

  solicitudes: Array<Solicitud>;

  constructor(
    private router: Router,
    private route: ActivatedRoute,
    private authService: AuthService,
    public dialog: MatDialog,
    public snackBar: MatSnackBar,
    private solicitudSisService: SolicitudSisService,
    ) { 
  }

  ngOnInit() {
    this.subscriptions.push(
      this.authService.getUsuario()
      .subscribe(
        response => {
          if (response) {
            this.usuario = response;
          }
        }
        )
      );
    this.subscriptions.push(
      this.solicitudSisService.index()
      .subscribe(solicitudes => {
        this.solicitudes = solicitudes;
        this.dataSource = new MatTableDataSource(this.solicitudes);
        this.dataSource.paginator = this.paginator;
        this.dataSource.sort = this.sort;
      })
      );
  }

  openRegistroSisDialog() {
    const dialogRef = this.dialog.open(RegistroSisComponent, {data: this.usuario});

    dialogRef.afterClosed().subscribe((solicitud) => {
      this.solicitudes.push(solicitud);
      this.snackBar.open('Se ha registrado la solicitud SIS #'+solicitud.id+' con éxito', '200 OK', {
        duration: 3000,
      });
      this.applyFilter();
    });
  }
  ngOnDestroy(){
    if (this.subscriptions) {
      this.subscriptions.forEach(subscription => {
        subscription.unsubscribe();
      });
    }
  }
  openDetalleSisDialog(solicitud: any) {
    const dialogRef = this.dialog.open(DetalleSisComponent, {data: solicitud});
    dialogRef.afterClosed().subscribe(result => {});
  }
  applyFilter() {
    this.dataSource.filter = this.busqueda.value.valor.trim().toLowerCase();
  }
}
