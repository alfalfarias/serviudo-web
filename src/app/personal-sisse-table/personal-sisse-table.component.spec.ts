import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PersonalSisseTableComponent } from './personal-sisse-table.component';

describe('PersonalSisseTableComponent', () => {
  let component: PersonalSisseTableComponent;
  let fixture: ComponentFixture<PersonalSisseTableComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PersonalSisseTableComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PersonalSisseTableComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
