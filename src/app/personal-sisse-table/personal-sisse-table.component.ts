import { Component, OnInit, ViewChild } from '@angular/core';
import { MatSort, MatTableDataSource, MatPaginator, MatSnackBar } from '@angular/material';
import { MatDialog } from '@angular/material';
import { animate, state, style, transition, trigger } from '@angular/animations';
import { SolicitudSisseService } from '../services/solicitudsisse.service';
import { Solicitud, Usuario } from '../app.model';
import { DetalleSisseComponent } from '../solicitud-interna/sisse/modals/detalle-sisse/detalle-sisse.component';
import * as moment from 'moment';
import 'moment/locale/pt-br';
import { Subscription } from 'rxjs';
import { FormControl, FormGroup } from '@angular/forms';
import { RegistroSisseComponent } from '../solicitud-interna/sisse/modals/registro-sisse/registro-sisse.component';
import { AuthService } from '../services/auth.service';

@Component({
  selector: 'app-personal-sisse-table',
  templateUrl: './personal-sisse-table.component.html',
  styleUrls: ['./personal-sisse-table.component.scss'],
})
export class PersonalSisseTableComponent implements OnInit {
  private subscriptions = new Array<Subscription>();
  displayedColumns: string[] = ['ID', 'Fecha', 'Asunto', 'Estado'];
  dataSource: MatTableDataSource<Solicitud>;
  fechaMoment: moment.Moment;
  public usuario: Usuario = new Usuario();

  busqueda: FormGroup = new FormGroup({
    filtro: new FormControl ('asunto'),
    valor: new FormControl ('')
  });

  @ViewChild(MatPaginator) paginator: MatPaginator;
  @ViewChild(MatSort) sort: MatSort;

  solicitudes: Array<Solicitud>;

  constructor(
    public dialog: MatDialog,
    private solicitudSisseService: SolicitudSisseService,
    public snackBar: MatSnackBar,
    private authService: AuthService,
    ) { }

  ngOnInit() {
    this.subscriptions.push(
      this.authService.getUsuario()
      .subscribe(
        response => {
          if (response) {
            this.usuario = response;
          }
        }
        )
      );
    // this.solicitudes = this.solicitudSisseService.solicitudesSisse;
    // this.dataSource = new MatTableDataSource(this.solicitudes);

    // this.fechaMoment = moment('2018-03-11 15:44:21');
    // moment.locale('es');
    this.subscriptions.push(
      this.solicitudSisseService.index()
      .subscribe(solicitudes => {
        console.log(this.solicitudes);
        
        this.solicitudes = solicitudes;
        this.dataSource = new MatTableDataSource(this.solicitudes);
        this.dataSource.paginator = this.paginator;
        this.dataSource.sort = this.sort;
      })
      );
  }

  // openDetalleSisseDialog(detail) {
  //   const dialogRef = this.dialog.open(DetalleSisseComponent, {data: detail});
  //   dialogRef.afterClosed().subscribe(result => {});
  //   // dialogRef.afterClosed().subscribe(result => {
  //   //   console.log(`Dialog result: ${result} Closed`);
  //   // });
  // }

  openRegistroSisseDialog() {
    const dialogRef = this.dialog.open(RegistroSisseComponent, {data: this.usuario});

    dialogRef.afterClosed().subscribe(result => {
      this.solicitudes.push(result);
      this.snackBar.open('Se ha registrado la solicitud SISSE #'+result.id+' con éxito', '200 OK', {
        duration: 3000,
      });
      this.applyFilter();
    });
  }

  applyFilter() {
    this.dataSource.filter = this.busqueda.value.valor.trim().toLowerCase();
  }

  ngOnDestroy(){
    if (this.subscriptions) {
      this.subscriptions.forEach(subscription => {
        subscription.unsubscribe();
      });
    }
  }
}