import { Pipe, PipeTransform } from '@angular/core';
import * as moment from 'moment';

moment.locale('es');

@Pipe({
  name: 'formatoFecha'
})
export class FormatoFechaPipe implements PipeTransform {

  transform(value: any, formato: string): any {
    return moment(value).format(formato);
  }

}
