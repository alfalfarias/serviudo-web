import { Pipe, PipeTransform } from '@angular/core';
import { SolicitudEstados } from '../app.model';

@Pipe({
    name: 'colorCategoria'
})
export class ColorCategoriaPipe implements PipeTransform {

    transform(value: any): any {
        return '#03DAC6';
    }
}

@Pipe({
    name: 'colorEstado'
})
export class ColorEstadoPipe implements PipeTransform {

    transform(value: any): any {
        const SolicitudEstado = SolicitudEstados.find( SolicitudEstado => SolicitudEstado.valor == value);
        return SolicitudEstado.background != null ? SolicitudEstado.background : '#000';
    }

}
interface colorSelector {
  estado:string;
  background: string;
  color: string;
}