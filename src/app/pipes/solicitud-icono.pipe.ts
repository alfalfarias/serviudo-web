import { Pipe, PipeTransform } from '@angular/core';
import { SolicitudEstados } from '../app.model';
import { SolicitudCategorias } from '../app.model';

@Pipe({
    name: 'solicitudIcono'
})
export class SolicitudIconoPipe implements PipeTransform {

    transform(value: any, campo?: string): any {
        if (campo == 'estado') {
            const SolicitudEstado = SolicitudEstados.find( SolicitudEstado => SolicitudEstado.valor == value);
            return SolicitudEstado.icon != null ? SolicitudEstado.icon : 'error';
        }
        if (campo == 'categoria') {
            const SolicitudCategoria = SolicitudCategorias.find( SolicitudCategoria => SolicitudCategoria.valor == value);
            return SolicitudCategoria.icon != null ? SolicitudCategoria.icon : 'error';
        }
        return null;
    }

}
