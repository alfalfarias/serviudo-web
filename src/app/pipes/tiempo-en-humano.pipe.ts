import { Pipe, PipeTransform } from '@angular/core';
import * as moment from 'moment';

moment.locale('es');

@Pipe({
    name: 'tiempoEnHumano',
    pure: false
})
export class TiempoEnHumanoPipe implements PipeTransform {
    transform(value:string) {
        return moment(value).fromNow();
    }
}

@Pipe({
    name: 'contadorTiempo',
    pure: false
})
export class ContadorTiempoPipe implements PipeTransform {
    transform(tiempo_en_proceso: number, updated_at: string): any {
        let duracion = tiempo_en_proceso;
        if (updated_at != null) {
            duracion += moment().diff(updated_at);
        }
        const s = Math.floor( (duracion/1000) % 60 );
        const m = Math.floor( (duracion/1000/60) % 60 );
        const h = Math.floor( (duracion/(1000*60*60)) % 24 );
        const d = Math.floor( duracion/(1000*60*60*24) );
        return d + ' días ' + h + ' horas ' + m + ' minutos y ' + s  + ' segundos.';   
    }
}
