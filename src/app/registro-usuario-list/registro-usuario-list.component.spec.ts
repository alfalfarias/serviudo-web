import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { RegistroUsuarioListComponent } from './registro-usuario-list.component';

describe('RegistroUsuarioListComponent', () => {
  let component: RegistroUsuarioListComponent;
  let fixture: ComponentFixture<RegistroUsuarioListComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ RegistroUsuarioListComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(RegistroUsuarioListComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
