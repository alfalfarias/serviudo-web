import { Component, OnInit } from '@angular/core';
import { Usuario } from '../app.model';
import { UsuariosService } from '../services/usuarios.service';

@Component({
  selector: 'app-registro-usuario-list',
  templateUrl: './registro-usuario-list.component.html',
  styleUrls: ['./registro-usuario-list.component.scss']
})
export class RegistroUsuarioListComponent implements OnInit {

  usuarios: Array<Usuario>;
  constructor(private UsuariosService: UsuariosService) { }

  ngOnInit() {
    this.usuarios = this.UsuariosService.usuarios;
  }
}
