export enum UsuarioDocumento {V = 'v', E = 'E', J = 'j'};
export enum UsuarioTipo {Personal = 'personal', Administrador = 'administrador'};
export enum UsuarioEstado {Pendiente = 'pendiente', Activo = 'activo', Bloqueado = 'bloqueado'};

export enum SolicitudAcceso {Publico = 'publico', Privado = 'privado'};
export enum SolicitudCategoria {AseoYLimpieza = 'aseo y limpieza', Jardineria = 'jardineria', Plomeria = 'plomeria', Electricidad = 'electricidad', Refrigeracion = 'refrigeracion', Pintura = 'pintura', Carpinteria = 'carpinteria', Herreria = 'herreria', Transporte = 'transporte'};
export enum SolicitudTipo {SIS = 'sis', SISSE = 'sisse'};
export enum SolicitudEstado {Pendiente = 'pendiente', Aprobado = 'aprobado', Rechazado = 'rechazado', Resuelto = 'resuelto', Inconcluso = 'inconcluso'};
export enum SolicitudPrioridad {Uno = 1, Dos = 2, Tres = 3, Cuatro = 4, Cinco = 5};
