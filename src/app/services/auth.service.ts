import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable, Subject, BehaviorSubject } from 'rxjs';
import { map, catchError } from 'rxjs/operators';
import { url } from '../globals';

import { JwtHelperService } from '@auth0/angular-jwt';
import { XhrErrorHandlerService } from './xhr-error-handler.service';

import { Usuario } from '../app.model';

@Injectable({
    providedIn: 'root'
})
export class AuthService {
    private usuarioBehaviorSubject: BehaviorSubject<Usuario> = new BehaviorSubject<Usuario>(new Usuario());
    usuario: Usuario = new Usuario();

    constructor(
        public http: HttpClient,
        private xhrErrorHandlerService: XhrErrorHandlerService,
        ) { 
    }
    getUsuario(): Observable<any> {
        return this.usuarioBehaviorSubject.asObservable();
    }
    setUsuario(usuario: Usuario) {
        this.usuarioBehaviorSubject.next(usuario);
    }

    login(loginParams: any): Observable<any>{
        return this.http.post(url+'login', loginParams)
        .pipe(
            map((response: any) => {
                localStorage.setItem('token', response.token);
                this.authenticate()
                .subscribe(
                    response => {
                        if (response) {
                            this.usuario = response;
                        }
                    }
                    );
                return response;
            })
            )
    }
    logout(): void {
        localStorage.removeItem('token');
        this.usuario = new Usuario();
        this.setUsuario(this.usuario);
    }
    authenticate(){
        return this.http.get<Usuario>(url + 'authenticate')
        .pipe(
            map((response: Usuario) => {
                this.usuario = response;
                this.setUsuario(this.usuario);
                return response;
            }
            ),
            catchError( error => {
                this.usuario = new Usuario();
                this.setUsuario(this.usuario);
                if (localStorage.getItem('token') != null) {
                    localStorage.removeItem('token');
                    throw(error);
                }
                else{
                    throw(null);
                }
            })
            )
    }
    getAuthorizationToken(): string {
        return localStorage.getItem('token');
    }
    getTokenDecoded(): any {
        return null;
    }
} 
