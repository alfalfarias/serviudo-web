import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Material } from '../app.model';
import { Service } from './service';

@Injectable({
  providedIn: 'root'
})
export class MaterialesService extends Service {
    public materiales = new Array<Material>();
    constructor(
        public http: HttpClient,
        ) { 
        super(http, 'materiales');
    }
}
