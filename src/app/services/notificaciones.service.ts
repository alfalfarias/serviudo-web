import { Injectable } from '@angular/core';
import { Notificacion } from '../app.model';

@Injectable({
    providedIn: 'root'
})
export class NotificacionesService {
    public notificaciones = new Array<Notificacion>();

    constructor() { 
        this.notificacionSeeder();
    }
    notificacionSeeder(){
        let notificacion = new Notificacion();
        notificacion.id = 1;
        notificacion.descripcion = 'Se ha aprobado tu solicitud SIS #24 "Falta de insumos en el comedor"';
        notificacion.created_at = '2018-10-20 00:14:59';
        notificacion.read_at = '2018-10-20 00:14:59';
        this.notificaciones.push(notificacion);

        notificacion.id = 1;
        notificacion.descripcion = 'Se ha rechazado tu solicitud SIS #32 "Falta de insumos del comedor"';
        notificacion.created_at = '2018-10-24 10:48:30';
        notificacion.read_at = '2018-10-24 10:48:30';
        this.notificaciones.push(notificacion);

        notificacion.id = 1;
        notificacion.descripcion = 'Se ha aprobado tu solicitud SIS #61 "Falta de insumos en el comedor"';
        notificacion.created_at = '2018-10-20 00:14:59';
        notificacion.read_at = '2018-10-24 23:25:12';
        this.notificaciones.push(notificacion);

        notificacion.id = 1;
        notificacion.descripcion = 'Se ha rechazado tu solicitud SIS #58 "Falta de insumos del comedor"';
        notificacion.created_at = '2018-10-25 07:13:14';
        notificacion.read_at = '2018-10-25 07:13:14';
        this.notificaciones.push(notificacion);

        notificacion.id = 1;
        notificacion.descripcion = 'Se ha aprobado tu solicitud SISSE #57 "Salida especial a PDVSA"';
        notificacion.created_at = '2018-10-20 00:14:59';
        notificacion.read_at = null;
        this.notificaciones.push(notificacion);

        notificacion.id = 1;
        notificacion.descripcion = 'Se ha aprobado tu solicitud SIS #56 "Limpieza de areas verdes Modulo II"';
        notificacion.created_at = '2018-10-20 00:14:59';
        notificacion.read_at = null;
        this.notificaciones.push(notificacion);
    }
}
