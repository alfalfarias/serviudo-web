import { Injectable } from '@angular/core';
import { Observable, Subject } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class ProgressBarService {

  private progressBarSubject = new Subject<ProgressBar>();
  progressBar = this.progressBarSubject.asObservable();

  constructor() { this.show(); }

  show() {
    this.progressBarSubject.next(<ProgressBar>{ show: true, mode: 'indeterminate' });
  }
  hide() {
    this.progressBarSubject.next(<ProgressBar>{ show: false, mode: 'determinate' });
  }

  setMode(progressBar: ProgressBar){
    this.progressBarSubject.next(progressBar);
  }
  getMode(): Observable<any> {
    return this.progressBarSubject.asObservable();
  }
}

export interface ProgressBar {
  show: boolean;
  mode: string;
}