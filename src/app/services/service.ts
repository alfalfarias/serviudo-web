import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';

import { Observable } from 'rxjs';
import { map, tap,  } from 'rxjs/operators';

import * as SELECTOR from '../selector.enums';
import { url } from '../globals';

export class Service {
    constructor(
        public http: HttpClient,
        protected endpoint: string,
        ) { 
    }
    index(params?: string): Observable<any> {
        return this.http.get<any>(url + this.getUri(params))
        .pipe(map(this.extractData));
    }
    store(data: any, params?: string): Observable<any> {
        return this.http.post<any>(url + this.getUri(params), (data))
        .pipe(map(this.extractData));
    }
    show(params?: string): Observable<any> {
        return this.http.get(url + this.getUri(params))
        .pipe(map(this.extractData));
    }
    update(data: any, params?: string): Observable<any> {
        return this.http.put(url + this.getUri(params), (data))
        .pipe(map(this.extractData));
    }
    delete (params?: string): Observable<any> {
        return this.http.delete<any>(url + this.getUri(params))
        .pipe(map(this.extractData));
    }

    protected extractData(res: Response) {
        let body = res;
        return body || { };
    }

    getUri(params: any){
        if (params == null) {
            return this.endpoint;
        }
        return params;
    }
}
