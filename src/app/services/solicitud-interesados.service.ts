import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';

import { Service } from './service';

import { SolicitudInteresado } from '../app.model';

@Injectable({
    providedIn: 'root'
})
export class SolicitudInteresadosService extends Service {
    public solicitudesSis = new Array<SolicitudInteresado>();
    constructor(
        public http: HttpClient,
        ) { 
        super(http, 'solicitud-interesados');
    }
}
