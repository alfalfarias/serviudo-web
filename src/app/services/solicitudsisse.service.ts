import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';

import { Service } from './service';

import { Solicitud } from '../app.model';

@Injectable({
  providedIn: 'root'
})
export class SolicitudSisseService extends Service {
    public solicitudesSisse = new Array<Solicitud>();
    constructor(
        public http: HttpClient,
        ) { 
        super(http, 'solicitud-sisse');
    }
}
