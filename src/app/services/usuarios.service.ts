import { Injectable } from '@angular/core';
import { Usuario } from '../app.model';
import { UsuarioDocumento, UsuarioTipo, UsuarioEstado } from '../selector.enums';

@Injectable({
  providedIn: 'root'
})
export class UsuariosService {
    public usuarios = new Array<Usuario>();
    constructor() { 
        this.materialSeeder();
    }

    materialSeeder(){
        let usuario = new Usuario();
        usuario.id = 1;
        usuario.cedula = 22718998;
        usuario.nombres = 'Simon Jose';
        usuario.apellidos = 'Farias Moreno';
        usuario.cargo = 'estudiante';
        usuario.correo_academico = '';
        usuario.correo_personal = '';
        usuario.correo_principal = '';
        usuario.descripcion = '';
        usuario.direccion = '';
        usuario.documento = UsuarioDocumento.V;
        usuario.estado = UsuarioEstado.Pendiente;
        usuario.tipo = UsuarioTipo.Personal;
        usuario.telefono = '';
        usuario.created_at = '';
        usuario.nacimiento = '';

        this.usuarios.push(usuario);
        usuario.id = 2;
        this.usuarios.push(usuario);
        usuario.id = 3;
        this.usuarios.push(usuario);
        usuario.id = 4;
        this.usuarios.push(usuario);
        usuario.id = 5;
        this.usuarios.push(usuario);
        usuario.id = 6;
        this.usuarios.push(usuario);
        usuario.id = 7;
        this.usuarios.push(usuario);
    }
}
