import { Injectable, ErrorHandler, Injector, NgZone } from '@angular/core';
import { HttpErrorResponse } from '@angular/common/http';
import { MatSnackBar } from '@angular/material';

@Injectable({
  providedIn: 'root'
})
export class XhrErrorHandlerService implements ErrorHandler{

  constructor(
    private injector: Injector,
    public snackBar: MatSnackBar,
    private readonly zone: NgZone
    ) {}

  handleError(error: Error | HttpErrorResponse){
    if (error instanceof HttpErrorResponse) {
      for (var i = 0; i < error.error.length; i++) {
        this.zone.run(() => {
          const snackBar = this.snackBar.open(error.error[i], error.status + ' OK', {
            verticalPosition: 'bottom',
            horizontalPosition: 'center',
            duration: 3000,
          });
          snackBar.onAction().subscribe(() => {
            snackBar.dismiss();
          })
        });
      }
      if (error.status == 0) {
        this.zone.run(() => {
          const snackBar = this.snackBar.open('No se pudo establecer conexión con el servidor', ' 500 OK', {
            verticalPosition: 'bottom',
            horizontalPosition: 'center',
            duration: 3000,
          });
          snackBar.onAction().subscribe(() => {
            snackBar.dismiss();
          })
        });
      }
    }
  }
}