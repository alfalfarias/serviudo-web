import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { InteresadosListaComponent } from './interesados-lista.component';

describe('InteresadosListaComponent', () => {
  let component: InteresadosListaComponent;
  let fixture: ComponentFixture<InteresadosListaComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ InteresadosListaComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(InteresadosListaComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
