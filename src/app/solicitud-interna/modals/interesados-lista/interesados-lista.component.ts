import { Component, OnInit, OnDestroy, Inject } from '@angular/core';
import { MatDialog, MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';

import { Subscription } from 'rxjs/Subscription';

import { SolicitudInteresado } from '../../../app.model';

@Component({
    selector: 'app-interesados-lista',
    templateUrl: './interesados-lista.component.html',
    styleUrls: ['./interesados-lista.component.scss']
})
export class InteresadosListaComponent implements OnInit, OnDestroy {

    private subscriptions = new Array<Subscription>();
    constructor(
        public dialogRef: MatDialogRef<InteresadosListaComponent>,
        @Inject(MAT_DIALOG_DATA) public solicitudInteresados: Array<SolicitudInteresado>,
        ) { }

    ngOnInit() {
    }
    ngOnDestroy(){
        if (this.subscriptions) {
            this.subscriptions.forEach(subscription => {
                subscription.unsubscribe();
            });
        }
    }
    onNoClick(): void {
        this.dialogRef.close();
    }

}
