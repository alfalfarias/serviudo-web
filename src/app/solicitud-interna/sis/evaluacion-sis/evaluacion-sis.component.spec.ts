import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { EvaluacionSisComponent } from './evaluacion-sis.component';

describe('EvaluacionSisComponent', () => {
  let component: EvaluacionSisComponent;
  let fixture: ComponentFixture<EvaluacionSisComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ EvaluacionSisComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(EvaluacionSisComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
