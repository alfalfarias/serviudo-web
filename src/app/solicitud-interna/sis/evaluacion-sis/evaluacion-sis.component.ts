import { Component, OnInit, OnDestroy, Input } from '@angular/core';
import { Subscription } from 'rxjs/Subscription';
import { Solicitud } from '../../../app.model';
import { MatDialog, MatSnackBar } from '@angular/material';

import { SolicitudInteresadosService } from '../../../services/solicitud-interesados.service';

import { InteresadosListaComponent } from '../../modals/interesados-lista/interesados-lista.component';
import { DetalleSisComponent } from '../modals/detalle-sis/detalle-sis.component';
import { AprobacionSisComponent } from '../modals/aprobacion-sis/aprobacion-sis.component';
import { RechazarSolicitudComponent } from '../../modals/rechazar-solicitud/rechazar-solicitud.component';
import { CargarResultadoSisComponent } from '../modals/cargar-resultado-sis/cargar-resultado-sis.component';

@Component({
  selector: 'app-evaluacion-sis',
  templateUrl: './evaluacion-sis.component.html',
  styleUrls: ['./evaluacion-sis.component.scss']
})
export class EvaluacionSisComponent implements OnInit, OnDestroy {

  @Input() solicitud:Solicitud;
  stars = Array<string>();
  private subscriptions = new Array<Subscription>();

  constructor(
    public dialog: MatDialog,
    private solicitudInteresadosService: SolicitudInteresadosService,
    public snackBar: MatSnackBar,
    ) {
   }

  ngOnInit() {
    for (let i = 1 ; i <= 5; i++) {
        this.stars.push('star_border');
    }
  }
  ngOnDestroy(){
    if (this.subscriptions) {
      this.subscriptions.forEach(subscription => {
        subscription.unsubscribe();
      });
    }
  }

  openInteresadosListaDialog() {
    if (this.solicitud.solicitudInteresados.length != 0) {
      this.subscriptions.push(this.solicitudInteresadosService.index('solicitudes/'+this.solicitud.id+'/interesados')
        .subscribe(
          solicitudInteresados => {
            const dialogRef = this.dialog.open(InteresadosListaComponent, {data: solicitudInteresados});
            dialogRef.afterClosed().subscribe(result => {});
          }
          )
        );
    }
    else{
      this.snackBar.open('Esta solicitud no continene interesados', '422 Error', {
        duration: 3000,
      });
    }

  }
  openDetalleSisDialog() {
    const dialogRef = this.dialog.open(DetalleSisComponent, {data: this.solicitud});
    dialogRef.afterClosed().subscribe(result => {});
  }

  openAprobarSisDialog() {
    const dialogRef = this.dialog.open(AprobacionSisComponent, {panelClass: 'test'});

    dialogRef.afterClosed().subscribe(result => {
      console.log(`Dialog result: ${result} Closed`);
    });
  }
  openRechazarSisDialog() {
    const dialogRef = this.dialog.open(RechazarSolicitudComponent, {panelClass: 'test'});

    dialogRef.afterClosed().subscribe(result => {
      console.log(`Dialog result: ${result} Closed`);
    });
  }
  openCargarResultadoSisDialog() {
    const dialogRef = this.dialog.open(CargarResultadoSisComponent, {panelClass: 'test'});

    dialogRef.afterClosed().subscribe(result => {
      console.log(`Dialog result: ${result} Closed`);
    });
  }
}
