import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { GestioneSisComponent } from './gestione-sis.component';

describe('GestioneSisComponent', () => {
  let component: GestioneSisComponent;
  let fixture: ComponentFixture<GestioneSisComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ GestioneSisComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(GestioneSisComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
