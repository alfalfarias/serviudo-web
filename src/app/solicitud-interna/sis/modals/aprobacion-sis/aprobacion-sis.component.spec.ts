import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AprobacionSisComponent } from './aprobacion-sis.component';

describe('AprobacionSisComponent', () => {
  let component: AprobacionSisComponent;
  let fixture: ComponentFixture<AprobacionSisComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AprobacionSisComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AprobacionSisComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
