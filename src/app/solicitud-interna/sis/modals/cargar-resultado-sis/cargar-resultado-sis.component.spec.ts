import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CargarResultadoSisComponent } from './cargar-resultado-sis.component';

describe('CargarResultadoSisComponent', () => {
  let component: CargarResultadoSisComponent;
  let fixture: ComponentFixture<CargarResultadoSisComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CargarResultadoSisComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CargarResultadoSisComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
