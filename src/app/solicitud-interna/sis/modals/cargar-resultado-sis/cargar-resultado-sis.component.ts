import { Component, OnInit, ViewChild } from '@angular/core';
import { Material } from '../../../../app.model';
import { MatSort, MatTableDataSource } from '@angular/material';
import { MaterialesService } from '../../../../services/materiales.service';

export interface PeriodicElement {
  name: string;
  position: number;
  weight: number;
  symbol: string;
}

@Component({
  selector: 'app-cargar-resultado-sis',
  templateUrl: './cargar-resultado-sis.component.html',
  styleUrls: ['./cargar-resultado-sis.component.scss']
})
export class CargarResultadoSisComponent implements OnInit {

  displayedColumns: string[] = ['ID', 'Nombre', 'Cantidad en Existencia', 'Cantidad solicitada'];
  dataSource: MatTableDataSource<Material>;

  @ViewChild(MatSort) sort: MatSort;

  materiales: Array<Material>;
  materialesTable: Array<Material>;

  constructor(private materialesService: MaterialesService) { }

  ngOnInit() {
    this.materiales = this.materialesService.materiales;
    this.materialesTable = new Array<Material>();
    this.dataSource = new MatTableDataSource(this.materialesTable);
  }

  addRowMaterial(){
    let material = new Material();
    material.id = 5;
    material.nombre = 'Clavo 3/4';
    material.descripcion = 'Kilo de clavo';
    material.cantidadMinimo = 10;
    material.cantidadExistencia = 200;
    material.created_at = '2018-09-25 20:03:50';
    material.updated_at = '2018-09-25 20:03:50';
    this.materialesTable.push(material);
    this.dataSource = new MatTableDataSource(this.materialesTable);
  }
}
