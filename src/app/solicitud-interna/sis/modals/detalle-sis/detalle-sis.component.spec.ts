import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DetalleSisComponent } from './detalle-sis.component';

describe('DetalleSisComponent', () => {
  let component: DetalleSisComponent;
  let fixture: ComponentFixture<DetalleSisComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DetalleSisComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DetalleSisComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
