import { Component, OnInit, OnDestroy, Inject, Pipe} from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';

import { Subscription } from 'rxjs/Subscription';

import * as moment from 'moment';

@Component({
    selector: 'app-detalle-sis',
    templateUrl: './detalle-sis.component.html',
    styleUrls: ['./detalle-sis.component.scss'],
})
export class DetalleSisComponent implements OnInit, OnDestroy {

    private subscriptions = new Array<Subscription>();
    public moment = moment;
    constructor(
        public dialogRef: MatDialogRef<DetalleSisComponent>,
        @Inject(MAT_DIALOG_DATA) public solicitud: any
        ) { 
    }

    ngOnInit() {
    }
    ngOnDestroy(){
        if (this.subscriptions) {
            this.subscriptions.forEach(subscription => {
                subscription.unsubscribe();
            });
        }
    }
    onNoClick(): void {
        this.dialogRef.close();
    }
}
