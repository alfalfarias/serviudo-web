import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { RegistroSisComponent } from './registro-sis.component';

describe('RegistroSisComponent', () => {
  let component: RegistroSisComponent;
  let fixture: ComponentFixture<RegistroSisComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ RegistroSisComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(RegistroSisComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
