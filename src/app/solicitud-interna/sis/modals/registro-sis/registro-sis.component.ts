import { Component, OnInit, OnDestroy, Inject, EventEmitter, ViewChild, ElementRef } from '@angular/core';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';
import { UploadOutput, UploadInput, UploadFile, humanizeBytes, UploaderOptions } from 'ngx-uploader';
import { Subscription } from 'rxjs/Subscription';
import { url } from "../../../../globals";
import { SolicitudSisService } from '../../../../services/solicitudsis.service';

import { Usuario, Solicitud, UsuarioDocumentos, SolicitudCategorias, SolicitudAccesos } from '../../../../app.model';

@Component({
    selector: 'app-registro-sis',
    templateUrl: './registro-sis.component.html',
    styleUrls: ['./registro-sis.component.scss']
})
export class RegistroSisComponent implements OnInit {
    @ViewChild('fileInput') fileInput:ElementRef;
    @ViewChild('img') img:ElementRef;
    
    options: UploaderOptions;
    formData: FormData;
    files: UploadFile[];
    uploadInput: EventEmitter<UploadInput>;
    // humanizeBytes: Function;
    dragOver: boolean;
    public uploaded: boolean = false;

    private subscriptions = new Array<Subscription>();
    public solicitud = new Solicitud();

    public UsuarioDocumentos = UsuarioDocumentos;
    public SolicitudCategorias = SolicitudCategorias;
    public SolicitudAccesos = SolicitudAccesos;

    formulario = new FormGroup({
        solicitante_id: new FormControl(this.usuario.id, []),
        solicitante_documento: new FormControl(this.usuario.documento, [ Validators.required,]),
        solicitante_cedula: new FormControl(this.usuario.cedula, [Validators.required]),
        solicitante_nombres: new FormControl(this.usuario.nombres, [Validators.required]),
        solicitante_apellidos: new FormControl(this.usuario.apellidos, [Validators.required]),
        solicitante_telefono: new FormControl(this.usuario.telefono, [Validators.required]),
        solicitante_correo: new FormControl(this.usuario.correo_principal, [Validators.required]),
        solicitante_cargo: new FormControl(this.usuario.cargo, []),
        solicitud_tipo: new FormControl('sis', [Validators.required]),
        acceso: new FormControl('publico', [Validators.required]),
        categoria: new FormControl(this.usuario.id, [Validators.required]),
        asunto: new FormControl('', [Validators.required]),
        descripcion: new FormControl('', [Validators.required]),
        estado: new FormControl('pendiente', [Validators.required]),
        solicitudSis: new FormGroup({
            foto_previa: new FormControl('', []),
            lugar: new FormControl('', [Validators.required, Validators.minLength(4)])
        })
    });

    constructor(
        public dialogRef: MatDialogRef<RegistroSisComponent>,
        private solicitudSisService: SolicitudSisService,
        @Inject(MAT_DIALOG_DATA) public usuario: Usuario
        ) {

        this.usuario = this.usuario;

        if (this.usuario != null) {
            this.formulario.patchValue({
                solicitante_documento: this.usuario.documento,
                solicitante_cedula: this.usuario.cedula,
                solicitante_nombres: this.usuario.nombres,
                solicitante_apellidos: this.usuario.apellidos,
                solicitante_telefono: this.usuario.telefono,
                solicitante_correo: this.usuario.correo_principal,
                solicitante_cargo: this.usuario.cargo,
            });
            if (this.usuario.tipo == 'personal') {
                this.formulario.get('solicitante_id').disable();
                this.formulario.get('solicitante_documento').disable();
                this.formulario.get('solicitante_cedula').disable();
                this.formulario.get('solicitante_nombres').disable();
                this.formulario.get('solicitante_apellidos').disable();
                this.formulario.get('solicitante_telefono').disable();
                this.formulario.get('solicitante_correo').disable();
                this.formulario.get('solicitante_cargo').disable();
            }
        }

        this.options = {
          concurrency: 1 ,
          allowedContentTypes: [
            "image/jpeg",
            "image/tiff",
            "image/png",
          ]};
         this.files = []; // local uploading files array
         this.uploadInput = new EventEmitter<UploadInput>();
    }

    onUploadOutput(output: UploadOutput): void {
      let token = localStorage.getItem('token');
      switch (output.type) {
        case 'allAddedToQueue':
            // uncomment this if you want to auto upload files when added
            // const event: UploadInput = {
            //   type: 'uploadAll',
            //   headers: { 'Authorization': 'Bearer ' + token },
            //   url: url + 'solicitud-sis/3/',
            //   method: 'POST',
            //   fieldName: 'foto_previa',
            //   data: { foo: 'bar' }
            // };
            // this.uploadInput.emit(event);
          break;
        case 'addedToQueue':
          if (typeof output.file !== 'undefined') {
            this.uploaded = true;
            
            this.files.push(output.file);
          }
          break;
        case 'uploading':
          if (typeof output.file !== 'undefined') {
            // update current data in files array for uploading file
            const index = this.files.findIndex((file) => typeof output.file !== 'undefined' && file.id === output.file.id);
            this.files[index] = output.file;
          }
          break;
        case 'removed':
          // remove file from array when removed
          this.files = this.files.filter((file: UploadFile) => file !== output.file);
          break;
        case 'dragOver':
          this.dragOver = true;
          break;
        case 'dragOut':
        case 'drop':
          this.dragOver = false;
          break;
        case 'done':
          // The file is downloaded
          break;
      }
    }
  
    startUpload(id:number): void {
      let token = localStorage.getItem('token');
      const event: UploadInput = {
        type: 'uploadAll',
        headers: { 'Authorization': 'Bearer ' + token },
        url: url + 'solicitud-sis/' + id + '/',
        method: 'POST',
        fieldName: 'foto_previa',
        // data: { foo: 'bar' }
      };
  
      this.uploadInput.emit(event);
    }
  
    cancelUpload(id: string): void {
      this.uploadInput.emit({ type: 'cancel', id: id });
    }
  
    removeFile(id: string): void {
      this.uploadInput.emit({ type: 'remove', id: id });
    }
  
    removeAllFiles(): void {
      this.uploadInput.emit({ type: 'removeAll' });
    }
    pickFile(event:Event){
      event.preventDefault();
      let el = this.fileInput.nativeElement.click();
      this.dragOver = false;
    }
 
    ngOnInit() {

    }
    ngOnDestroy(){
        if (this.subscriptions) {
            this.subscriptions.forEach(subscription => {
                subscription.unsubscribe();
            });
        }
    }
    public registrar(){
        this.formulario.enable();
        this.subscriptions.push(this.solicitudSisService.store(this.formulario.value)
            .subscribe(
                response => {
                  this.startUpload(response.id)
                    this.solicitud = response;
                    this.dialogRef.close(this.solicitud);
                }
                )
            );
    }
}