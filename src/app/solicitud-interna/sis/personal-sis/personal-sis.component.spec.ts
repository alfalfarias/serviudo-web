import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PersonalSisComponent } from './personal-sis.component';

describe('PersonalSisComponent', () => {
  let component: PersonalSisComponent;
  let fixture: ComponentFixture<PersonalSisComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PersonalSisComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PersonalSisComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
