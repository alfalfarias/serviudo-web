import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SimpleSisComponent } from './simple-sis.component';

describe('SimpleSisComponent', () => {
  let component: SimpleSisComponent;
  let fixture: ComponentFixture<SimpleSisComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SimpleSisComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SimpleSisComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
