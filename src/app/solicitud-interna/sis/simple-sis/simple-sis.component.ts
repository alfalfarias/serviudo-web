import { Component, OnInit, AfterViewInit, OnDestroy, Input } from '@angular/core';
import { MatDialog } from '@angular/material';
import { MatSnackBar } from '@angular/material';

import { Subscription } from 'rxjs/Subscription';

import { AuthService } from '../../../services/auth.service';
import { SolicitudInteresadosService } from '../../../services/solicitud-interesados.service';

import { Usuario, Solicitud } from '../../../app.model';

import { InteresadosListaComponent } from '../../modals/interesados-lista/interesados-lista.component';
import { DetalleSisComponent } from '../modals/detalle-sis/detalle-sis.component';

@Component({
  selector: 'app-simple-sis',
  templateUrl: './simple-sis.component.html',
  styleUrls: ['../../solicitud-interna.component.scss']
})
export class SimpleSisComponent implements OnInit, AfterViewInit, OnDestroy {

  @Input() solicitud:Solicitud;
  usuario: Usuario = new Usuario();
  enSeguimiento: boolean = false;
  private subscriptions = new Array<Subscription>();
  stars = Array<string>();

  constructor(
    public dialog: MatDialog,
    private authService: AuthService,
    private solicitudInteresadosService: SolicitudInteresadosService,
    public snackBar: MatSnackBar,
    ) {
  }

  ngOnInit() {
    for (let i = 1 ; i <= 5; i++) {
      if (this.solicitud.prioridad < i) {
        this.stars.push('star_border');
      }
      else{
        this.stars.push('star');
      }
    }
    this.subscriptions.push(
      this.authService.getUsuario().subscribe((usuario) => {
        this.usuario = usuario;
        this.enSeguimiento = this.solicitud.solicitudInteresados.some(interesado => interesado.usuario_id == this.usuario.id);
      })
      );
  }
  ngAfterViewInit() {
  }
  ngOnDestroy(){
    if (this.subscriptions) {
      this.subscriptions.forEach(subscription => {
        subscription.unsubscribe();
      });
    }
  }

  openInteresadosListaDialog() {
    if (this.solicitud.solicitudInteresados.length != 0) {
      this.subscriptions.push(this.solicitudInteresadosService.index('solicitudes/'+this.solicitud.id+'/interesados')
        .subscribe(
          solicitudInteresados => {
            const dialogRef = this.dialog.open(InteresadosListaComponent, {data: solicitudInteresados});
            dialogRef.afterClosed().subscribe(result => {});
          }
          )
        );
    }
    else{
      this.snackBar.open('Esta solicitud no continene interesados', '422 Error', {
        duration: 3000,
      });
    }

  }
  openDetalleSisDialog() {
    const dialogRef = this.dialog.open(DetalleSisComponent, {data: this.solicitud});
    dialogRef.afterClosed().subscribe(result => {});
  }

  unirse(){
    const solicitudInteresado = {
      usuario_id: this.usuario.id,
      solicitud_id: this.solicitud.id
    };
    this.subscriptions.push(this.solicitudInteresadosService.store(solicitudInteresado, 'solicitudes/'+this.solicitud.id+'/interesados')
      .subscribe(
        solicitudInteresado => {
          this.solicitud.solicitudInteresados.push(solicitudInteresado);
          this.enSeguimiento = true;
        }
        )
      );
  }
  desunirse() {
    const solicitudInteresado = this.solicitud.solicitudInteresados.find(solicitudInteresado => solicitudInteresado.usuario_id == this.usuario.id);
    this.subscriptions.push(this.solicitudInteresadosService.delete('solicitudes/'+this.solicitud.id+'/interesados/'+this.usuario.id)
      .subscribe(
        solicitudInteresado => {
          this.solicitud.solicitudInteresados = this.solicitud.solicitudInteresados
          .filter( solicitudInteresado => solicitudInteresado.usuario_id != this.usuario.id);
          this.enSeguimiento = false;
        }
        )
      );
  }
}