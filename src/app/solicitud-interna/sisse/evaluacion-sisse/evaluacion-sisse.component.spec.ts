import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { EvaluacionSisseComponent } from './evaluacion-sisse.component';

describe('EvaluacionSisseComponent', () => {
  let component: EvaluacionSisseComponent;
  let fixture: ComponentFixture<EvaluacionSisseComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ EvaluacionSisseComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(EvaluacionSisseComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
