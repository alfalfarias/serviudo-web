import { Component, OnInit, Input } from '@angular/core';
import { Subscription } from 'rxjs/Subscription';
import { Solicitud } from '../../../app.model';
import { MatDialog, MatSnackBar } from '@angular/material';

import { SolicitudInteresadosService } from '../../../services/solicitud-interesados.service';
import { InteresadosListaComponent } from '../../modals/interesados-lista/interesados-lista.component';
import { DetalleSisseComponent } from '../modals/detalle-sisse/detalle-sisse.component';
import { AprobacionSisseComponent } from '../modals/aprobacion-sisse/aprobacion-sisse.component';
import { RechazarSolicitudComponent } from '../../modals/rechazar-solicitud/rechazar-solicitud.component';
import { CargarResultadoSisseComponent } from '../modals/cargar-resultado-sisse/cargar-resultado-sisse.component';

@Component({
  selector: 'app-evaluacion-sisse',
  templateUrl: './evaluacion-sisse.component.html',
  styleUrls: ['./evaluacion-sisse.component.scss']
})
export class EvaluacionSisseComponent implements OnInit {

  @Input() solicitud:Solicitud;
  stars = Array<string>();
  private subscriptions = new Array<Subscription>();

  constructor(
    public dialog: MatDialog,
    private solicitudInteresadosService: SolicitudInteresadosService,
    public snackBar: MatSnackBar,
  ) {
   }

  ngOnInit() {
    for (let i = 1 ; i <= 5; i++) {
        this.stars.push('star_border');
    }
  }

  
  openInteresadosListaDialog() {
    if (this.solicitud.solicitudInteresados.length != 0) {
      this.subscriptions.push(this.solicitudInteresadosService.index('solicitudes/'+this.solicitud.id+'/interesados')
        .subscribe(
          solicitudInteresados => {
            const dialogRef = this.dialog.open(InteresadosListaComponent, {data: solicitudInteresados});
            dialogRef.afterClosed().subscribe(result => {});
          }
          )
        );
    }
    else{
      this.snackBar.open('Esta solicitud no continene interesados', '422 Error', {
        duration: 3000,
      });
    }

  }
  openDetalleSisseDialog() {
    const dialogRef = this.dialog.open(DetalleSisseComponent, {panelClass: 'test'});

    dialogRef.afterClosed().subscribe(result => {
      console.log(`Dialog result: ${result} Closed`);
    });
  }

  openAprobarSisseDialog() {
    const dialogRef = this.dialog.open(AprobacionSisseComponent, {panelClass: 'test'});

    dialogRef.afterClosed().subscribe(result => {
      console.log(`Dialog result: ${result} Closed`);
    });
  }
  openRechazarSisseDialog() {
    const dialogRef = this.dialog.open(RechazarSolicitudComponent, {panelClass: 'test'});

    dialogRef.afterClosed().subscribe(result => {
      console.log(`Dialog result: ${result} Closed`);
    });
  }
  openCargarResultadoSisseDialog() {
    const dialogRef = this.dialog.open(CargarResultadoSisseComponent, {panelClass: 'test'});

    dialogRef.afterClosed().subscribe(result => {
      console.log(`Dialog result: ${result} Closed`);
    });
  }

}
