import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { GestionSisseComponent } from './gestion-sisse.component';

describe('GestionSisseComponent', () => {
  let component: GestionSisseComponent;
  let fixture: ComponentFixture<GestionSisseComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ GestionSisseComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(GestionSisseComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
