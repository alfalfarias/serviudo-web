import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AprobacionSisseComponent } from './aprobacion-sisse.component';

describe('AprobacionSisseComponent', () => {
  let component: AprobacionSisseComponent;
  let fixture: ComponentFixture<AprobacionSisseComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AprobacionSisseComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AprobacionSisseComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
