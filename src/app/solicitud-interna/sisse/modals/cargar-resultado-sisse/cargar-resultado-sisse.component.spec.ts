import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CargarResultadoSisseComponent } from './cargar-resultado-sisse.component';

describe('CargarResultadoSisseComponent', () => {
  let component: CargarResultadoSisseComponent;
  let fixture: ComponentFixture<CargarResultadoSisseComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CargarResultadoSisseComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CargarResultadoSisseComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
