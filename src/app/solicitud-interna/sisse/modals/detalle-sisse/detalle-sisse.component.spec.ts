import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DetalleSisseComponent } from './detalle-sisse.component';

describe('DetalleSisseComponent', () => {
  let component: DetalleSisseComponent;
  let fixture: ComponentFixture<DetalleSisseComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DetalleSisseComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DetalleSisseComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
