import { Component, OnInit, Inject } from '@angular/core';
import { MAT_DIALOG_DATA, MatDialogRef } from '@angular/material';
import { Subscription } from 'rxjs';

@Component({
  selector: 'app-detalle-sisse',
  templateUrl: './detalle-sisse.component.html',
  styleUrls: ['./detalle-sisse.component.scss']
})
export class DetalleSisseComponent implements OnInit {
  private subscriptions = new Array<Subscription>();

  constructor(
    public dialogRef: MatDialogRef<DetalleSisseComponent>,
    @Inject(MAT_DIALOG_DATA) public detail: any,
  ) { }

  ngOnInit() {
    console.log(this.detail);

  }

  onNoClick(): void {
    this.dialogRef.close();
  }

  ngOnDestroy() {
    if (this.subscriptions) {
      this.subscriptions.forEach(subscription => {
        subscription.unsubscribe();
      });
    }
  }

}
