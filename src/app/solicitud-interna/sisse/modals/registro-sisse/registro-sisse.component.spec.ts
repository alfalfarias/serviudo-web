import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { RegistroSisseComponent } from './registro-sisse.component';

describe('RegistroSisseComponent', () => {
  let component: RegistroSisseComponent;
  let fixture: ComponentFixture<RegistroSisseComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ RegistroSisseComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(RegistroSisseComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
