import { Component, OnInit, Inject } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';
import { Usuario, SolicitudCategorias, UsuarioDocumentos, SolicitudAccesos, Solicitud } from 'src/app/app.model';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { Subscription } from 'rxjs';
import { SolicitudSisseService } from 'src/app/services/solicitudsisse.service';
import { DatePipe } from '@angular/common';

@Component({
  selector: 'app-registro-sisse',
  templateUrl: './registro-sisse.component.html',
  styleUrls: ['./registro-sisse.component.scss']
})
export class RegistroSisseComponent implements OnInit {
  private subscriptions = new Array<Subscription>();
  public solicitud = new Solicitud();

  public UsuarioDocumentos = UsuarioDocumentos;
  public SolicitudCategorias = SolicitudCategorias;
  public SolicitudAccesos = SolicitudAccesos;

  formulario = new FormGroup({
    solicitante_id: new FormControl(this.usuario.id, []),
    solicitante_documento: new FormControl(this.usuario.documento, [Validators.required,]),
    solicitante_cedula: new FormControl(this.usuario.cedula, [Validators.required]),
    solicitante_nombres: new FormControl(this.usuario.nombres, [Validators.required]),
    solicitante_apellidos: new FormControl(this.usuario.apellidos, [Validators.required]),
    solicitante_telefono: new FormControl(this.usuario.telefono, [Validators.required]),
    solicitante_correo: new FormControl(this.usuario.correo_principal, [Validators.required]),
    solicitante_cargo: new FormControl(this.usuario.cargo, []),
    solicitud_tipo: new FormControl('sisse', [Validators.required]),
    acceso: new FormControl('publico', [Validators.required]),
    categoria: new FormControl('transporte', [Validators.required]),
    asunto: new FormControl('', [Validators.required]),
    descripcion: new FormControl('', [Validators.required]),
    estado: new FormControl('pendiente', [Validators.required]),
    solicitudSisse: new FormGroup({
      fecha_salida1: new FormControl('', []),
      fecha_salida2: new FormControl('', []),
      fecha_regreso1: new FormControl('', []),
      fecha_regreso2: new FormControl('', []),
      lugar_salida: new FormControl('', []),
      lugar_llegada: new FormControl('', []),
      cantidad_personas: new FormControl( '', []),
      // chofer_asignado: new FormControl('', []),
      // lugar: new FormControl('', [Validators.required, Validators.minLength(4)])
    })
  });

  constructor(
    public dialogRef: MatDialogRef<RegistroSisseComponent>,
    private solicitudSisseService: SolicitudSisseService,
    private datePipe: DatePipe,
    @Inject(MAT_DIALOG_DATA) public usuario: Usuario
  ) {

    this.usuario = this.usuario;

    if (this.usuario != null) {
      this.formulario.patchValue({
        solicitante_documento: this.usuario.documento,
        solicitante_cedula: this.usuario.cedula,
        solicitante_nombres: this.usuario.nombres,
        solicitante_apellidos: this.usuario.apellidos,
        solicitante_telefono: this.usuario.telefono,
        solicitante_correo: this.usuario.correo_principal,
        solicitante_cargo: this.usuario.cargo,
      });
      if (this.usuario.tipo == 'personal') {
        this.formulario.get('solicitante_id').disable();
        this.formulario.get('solicitante_documento').disable();
        this.formulario.get('solicitante_cedula').disable();
        this.formulario.get('solicitante_nombres').disable();
        this.formulario.get('solicitante_apellidos').disable();
        this.formulario.get('solicitante_telefono').disable();
        this.formulario.get('solicitante_correo').disable();
        this.formulario.get('solicitante_cargo').disable();
      }
    }
  }

  ngOnInit() {
  }

  public registrar(){
    this.formulario.enable();
    console.log(this.formulario.value);
    // this.datePipe.transform(new Date());
    this.formulario.value.solicitudSisse.fecha_salida = this.datePipe.transform(this.formulario.value.solicitudSisse.fecha_salida1, 'yyyy-MM-dd') + ' ' + this.formulario.value.solicitudSisse.fecha_salida2;
    this.formulario.value.solicitudSisse.fecha_regreso = this.datePipe.transform(this.formulario.value.solicitudSisse.fecha_regreso1, 'yyyy-MM-dd') + ' ' + this.formulario.value.solicitudSisse.fecha_regreso2;
    
    this.subscriptions.push(this.solicitudSisseService.store(this.formulario.value)
        .subscribe(
            response => {
                this.solicitud = response;
                this.dialogRef.close(this.solicitud);
            }
            )
        );
}

}
