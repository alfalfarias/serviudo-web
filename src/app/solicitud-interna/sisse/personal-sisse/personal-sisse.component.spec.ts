import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PersonalSisseComponent } from './personal-sisse.component';

describe('PersonalSisseComponent', () => {
  let component: PersonalSisseComponent;
  let fixture: ComponentFixture<PersonalSisseComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PersonalSisseComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PersonalSisseComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
