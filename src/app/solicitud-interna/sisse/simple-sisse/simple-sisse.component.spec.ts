import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SimpleSisseComponent } from './simple-sisse.component';

describe('SimpleSisseComponent', () => {
  let component: SimpleSisseComponent;
  let fixture: ComponentFixture<SimpleSisseComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SimpleSisseComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SimpleSisseComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
