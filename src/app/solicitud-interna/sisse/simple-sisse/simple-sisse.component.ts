import { Component, OnInit, Input } from '@angular/core';
import { Solicitud, Usuario } from '../../../app.model';
import { MatDialog, MatSnackBar } from '@angular/material';
import { InteresadosListaComponent } from '../../modals/interesados-lista/interesados-lista.component';
import { DetalleSisseComponent } from '../modals/detalle-sisse/detalle-sisse.component';
import { Subscription } from 'rxjs';
import { SolicitudInteresadosService } from 'src/app/services/solicitud-interesados.service';
import { AuthService } from 'src/app/services/auth.service';

@Component({
  selector: 'app-simple-sisse',
  templateUrl: './simple-sisse.component.html',
  styleUrls: ['../../solicitud-interna.component.scss']
})
export class SimpleSisseComponent implements OnInit {
    
  @Input() solicitud:Solicitud;
  stars = Array<string>();
  private subscriptions = new Array<Subscription>();
  usuario: Usuario = new Usuario();
  enSeguimiento: boolean = false;

  constructor(
    public dialog: MatDialog,
    public snackBar: MatSnackBar,
    private solicitudInteresadosService: SolicitudInteresadosService,
    private authService: AuthService,
    ) {
  }

  ngOnInit() {
    
    for (let i = 1 ; i <= 5; i++) {
      if (this.solicitud.prioridad < i) {
        this.stars.push('star_border');
      }
      else{
        this.stars.push('star');
      }
    }

    this.subscriptions.push(
      this.authService.getUsuario().subscribe((usuario) => {
        this.usuario = usuario;
        this.enSeguimiento = this.solicitud.solicitudInteresados.some(interesado => interesado.usuario_id == this.usuario.id);
      })
      );
  }
  
  openInteresadosListaDialog() {
    // const dialogRef = this.dialog.open(InteresadosListaComponent, {panelClass: 'test'});

    // dialogRef.afterClosed().subscribe(result => {
    //   console.log(`Dialog result: ${result} Closed`);
    // });
    if (this.solicitud.solicitudInteresados.length != 0) {
      this.subscriptions.push(this.solicitudInteresadosService.index('solicitudes/'+this.solicitud.id+'/interesados')
        .subscribe(
          solicitudInteresados => {
            console.log(solicitudInteresados);
            
            const dialogRef = this.dialog.open(InteresadosListaComponent, {data: solicitudInteresados});
            dialogRef.afterClosed().subscribe(result => {});
          }
          )
        );
    }
    else{
      this.snackBar.open('Esta solicitud no continene interesados', '422 Error', {
        duration: 3000,
      });
    }
  }
  openDetalleSisseDialog() {
    const dialogRef = this.dialog.open(DetalleSisseComponent, {data: this.solicitud});
    dialogRef.afterClosed().subscribe(result => {});
    // dialogRef.afterClosed().subscribe(result => {
    //   console.log(`Dialog result: ${result} Closed`);
    // });
  }

  unirse(){
    const solicitudInteresado = {
      usuario_id: this.usuario.id,
      solicitud_id: this.solicitud.id
    };
    this.subscriptions.push(this.solicitudInteresadosService.store(solicitudInteresado, 'solicitudes/'+this.solicitud.id+'/interesados')
      .subscribe(
        solicitudInteresado => {
          this.solicitud.solicitudInteresados.push(solicitudInteresado);
          this.enSeguimiento = true;
        }
        )
      );
  }
  desunirse() {
    const solicitudInteresado = this.solicitud.solicitudInteresados.find(solicitudInteresado => solicitudInteresado.usuario_id == this.usuario.id);
    this.subscriptions.push(this.solicitudInteresadosService.delete('solicitudes/'+this.solicitud.id+'/interesados/'+this.usuario.id)
      .subscribe(
        solicitudInteresado => {
          this.solicitud.solicitudInteresados = this.solicitud.solicitudInteresados
          .filter( solicitudInteresado => solicitudInteresado.usuario_id != this.usuario.id);
          this.enSeguimiento = false;
        }
        )
      );
  }

}

