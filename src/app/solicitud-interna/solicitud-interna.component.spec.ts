import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SolicitudInternaComponent } from './solicitud-interna.component';

describe('SolicitudComponent', () => {
  let component: SolicitudInternaComponent;
  let fixture: ComponentFixture<SolicitudInternaComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SolicitudInternaComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SolicitudInternaComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
