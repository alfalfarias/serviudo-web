import { Component, OnInit, AfterViewInit, OnDestroy } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { MatDialog, PageEvent, MatPaginator, MatSnackBar } from '@angular/material';

import { Subscription } from 'rxjs/Subscription';

import { AuthService } from '../services/auth.service';
import { SolicitudSisService } from '../services/solicitudsis.service';
import { SolicitudSisseService } from '../services/solicitudsisse.service';

import { RegistroSisComponent } from './sis/modals/registro-sis/registro-sis.component';
import { RegistroSisseComponent } from './sisse/modals/registro-sisse/registro-sisse.component';

import { Usuario } from '../app.model';
import { Solicitud } from '../app.model';

@Component({
  selector: 'app-solicitud-interna',
  templateUrl: './solicitud-interna.component.html',
  styleUrls: ['./solicitud-interna.component.scss']
})
export class SolicitudInternaComponent implements OnInit, AfterViewInit, OnDestroy {

  public usuario: Usuario = new Usuario();
  private subscriptions: Array<Subscription> = new Array<Subscription>();
  private solicitudes: Array<Solicitud> = new Array<Solicitud>();
  private solicitudService: any;

  solicitudTipo: string;

  busqueda: FormGroup = new FormGroup({
    filtro: new FormControl ('asunto'),
    valor: new FormControl ('')
  });

  public dataSource: Array<Solicitud> = new Array<Solicitud>();

  pagination: any = {
    length: 10,
    pageSize: 10,
    pageIndex: 0,
    pageSizeOptions: [5, 10, 25, 100]
  };
  pageEvent: PageEvent = new PageEvent();

  constructor(
    private route: ActivatedRoute, 
    private authService: AuthService,
    private solicitudSisService: SolicitudSisService,
    private solicitudSisseService: SolicitudSisseService,
    public dialog: MatDialog,
    public snackBar: MatSnackBar,
    ) { 
  }

  ngOnInit() {
    this.subscriptions.push(
      this.authService.getUsuario()
      .subscribe(
        response => {
          if (response) {
            this.usuario = response;
          }
        }
        )
      );

    if (this.solicitudTipo == 'sis') {
      this.solicitudService = this.solicitudSisService;
    }
    else {
      this.solicitudService = this.solicitudSisseService;
    }
    this.route.params.subscribe(params => {
      this.solicitudTipo = params.type;
      if (this.solicitudTipo == 'sis') {
        this.solicitudService = this.solicitudSisService;
      }
      else {
        this.solicitudService = this.solicitudSisseService;
      }
      this.applyFilter();
    });
    
    this.subscriptions.push(
      this.solicitudService.index()
      .subscribe(solicitudes => {
        this.solicitudes = solicitudes.sort((a, b) => b.id - a.id);
        this.dataSource = this.solicitudes.slice(0, this.pagination.pageSize);

        this.pagination.length = this.solicitudes.length;

        this.pageEvent.length = this.solicitudes.length;
        this.pageEvent.pageSize = this.pagination.pageSize;
        this.pageEvent.pageIndex = this.pagination.pageIndex;
        this.refrescarPaginacion(this.pageEvent);
      })
      );
  }

  ngAfterViewInit() {
    const interval = 1000;
    setInterval(() => {
      for (var i = 0; i >= this.dataSource.length; i++) {
        this.dataSource[i].tiempo_aprobado += interval;
      }
    }, interval);
  }

  ngOnDestroy(){
    if (this.subscriptions) {
      this.subscriptions.forEach(subscription => {
        subscription.unsubscribe();
      });
    }
  }

  openRegistroSisDialog() {
    const dialogRef = this.dialog.open(RegistroSisComponent, {data: this.usuario});

    dialogRef.afterClosed().subscribe((solicitud) => {
      this.solicitudes.push(solicitud);
      this.snackBar.open('Se ha registrado la solicitud SIS #'+solicitud.id+' con éxito', '200 OK', {
        duration: 3000,
      });
      this.applyFilter();
    });
  }
  openRegistroSisseDialog() {
    const dialogRef = this.dialog.open(RegistroSisseComponent, {data: this.usuario});

    dialogRef.afterClosed().subscribe(result => {
      this.solicitudes.push(result);
      this.snackBar.open('Se ha registrado la solicitud SISSE #'+result.id+' con éxito', '200 OK', {
        duration: 3000,
      });
      this.applyFilter();
    });
  }

  refrescarPaginacion(pageEvent?:PageEvent){
    this.pageEvent = pageEvent;
    this.pageEvent.length = this.solicitudes.length;
    const start: number = this.pageEvent.pageIndex * this.pageEvent.pageSize;
    const end: number = start + this.pageEvent.pageSize;
    this.dataSource = this.solicitudes.slice(start, end);
  }

  applyFilter() {
    const value: string = this.busqueda.value.valor.trim().toLowerCase();

    this.subscriptions.push(
      this.solicitudService.index()
      .subscribe(solicitudes => {
        this.solicitudes = solicitudes.sort((a, b) => b.id - a.id);

        var filtros = ['id', 'asunto', 'categoria', 'estado', 'descripcion', 'solicitante_nombres', 'solicitante_apellidos', 'solicitante_cedula'];

        this.solicitudes = this.solicitudes.filter(solicitud  => {
          if ( solicitud.solicitud_tipo === this.solicitudTipo && (
            solicitud.id.toString().toLowerCase().includes(value) ||
            solicitud.asunto.toLowerCase().includes(value) || 
            solicitud.categoria.toLowerCase().includes(value) || 
            solicitud.estado.toLowerCase().includes(value) || 
            solicitud.descripcion.toLowerCase().includes(value) || 
            (solicitud.solicitante_nombres + ' ' + solicitud.solicitante_apellidos).toString().toLowerCase().includes(value))
            ){
            return true;
        }
        return false;
      });

        this.pagination.length = this.solicitudes.length;

        this.refrescarPaginacion(this.pageEvent);
      })
      );
  }
}
