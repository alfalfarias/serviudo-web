import { Component, OnInit, OnDestroy, Inject, EventEmitter, ViewChild, ElementRef } from '@angular/core';
import { FormGroup, FormControl, Validators } from '@angular/forms';

import { MatSnackBar } from '@angular/material';

import { UploadOutput, UploadInput, UploadFile, humanizeBytes, UploaderOptions } from 'ngx-uploader';

import { Subscription } from 'rxjs/Subscription';

import { AuthService } from '../../services/auth.service';

import { Usuario } from '../../app.model';
import { url } from "./../../globals";

@Component({
  selector: 'app-configuracion-cuenta',
  templateUrl: './configuracion-cuenta.component.html',
  styleUrls: ['./configuracion-cuenta.component.scss']
})
export class ConfiguracionCuentaComponent implements OnInit, OnDestroy {
  @ViewChild('fileInput') fileInput:ElementRef;
  @ViewChild('img') img:ElementRef;

  options: UploaderOptions;
  formData: FormData;
  files: UploadFile[];
  uploadInput: EventEmitter<UploadInput>;
  // humanizeBytes: Function;
  dragOver: boolean;
  public uploaded: boolean = false;

  private subscriptions = new Array<Subscription>();
  public usuario: Usuario = new Usuario();
  step = 0;
  
  formulario = new FormGroup({
    datosUsuario: new FormGroup({
      avatar: new FormControl(this.usuario.avatar, []),
      cargo: new FormControl(this.usuario.cargo, []),
      descripcion: new FormControl('', [Validators.required, Validators.minLength(4)])
    })
});
  constructor(
    private authService: AuthService,
    private snackBar: MatSnackBar
  ) { }


  ngOnInit() {
    this.subscriptions.push(
        this.authService.getUsuario()
        .subscribe(usuario => {
            this.usuario = usuario;
            
            this.formulario.patchValue({
              datosUsuario: {
                avatar: this.usuario.avatar,
                cargo: this.usuario.cargo,
                descripcion: this.usuario.descripcion,
              }
          });
        })
        );
  }
  ngOnDestroy(){
      if (this.subscriptions) {
          this.subscriptions.forEach(subscription => {
              subscription.unsubscribe();
          });
      }
  }

  onUploadOutput(output: UploadOutput): void {
    let token = localStorage.getItem('token');
    switch (output.type) {
      case 'allAddedToQueue':
        break;
      case 'addedToQueue':
        if (typeof output.file !== 'undefined') {
          this.uploaded = true;
          
          this.files.push(output.file);
        }
        break;
      case 'uploading':
        if (typeof output.file !== 'undefined') {
          // update current data in files array for uploading file
          const index = this.files.findIndex((file) => typeof output.file !== 'undefined' && file.id === output.file.id);
          this.files[index] = output.file;
        }
        break;
      case 'removed':
        // remove file from array when removed
        this.files = this.files.filter((file: UploadFile) => file !== output.file);
        break;
      case 'dragOver':
        this.dragOver = true;
        break;
      case 'dragOut':
      case 'drop':
        this.dragOver = false;
        break;
      case 'done':
        // The file is downloaded
        break;
    }
  }

  startUpload(id:number): void {
    let token = localStorage.getItem('token');
    const event: UploadInput = {
      type: 'uploadAll',
      headers: { 'Authorization': 'Bearer ' + token },
      url: url + 'usuarios/' + id + '/',
      method: 'POST',
      fieldName: 'avatar',
      // data: { foo: 'bar' }
    };

    this.uploadInput.emit(event);
  }

  cancelUpload(id: string): void {
    this.uploadInput.emit({ type: 'cancel', id: id });
  }

  removeFile(id: string): void {
    this.uploadInput.emit({ type: 'remove', id: id });
  }

  removeAllFiles(): void {
    this.uploadInput.emit({ type: 'removeAll' });
  }
  pickFile(event:Event){
    event.preventDefault();
    let el = this.fileInput.nativeElement.click();
    this.dragOver = false;
  }
  setStep(index: number) {
    this.step = index;
  }

  nextStep() {
    this.step++;
  }

  prevStep() {
    this.step--;
  }
  otroCargo(): string{
    if (this.formulario.value.datosUsuario.cargo != 'estudiante' && this.formulario.value.datosUsuario.cargo != 'profesor' && 
    this.formulario.value.datosUsuario.cargo != 'personal administrativo' && this.formulario.value.datosUsuario.cargo != 'obrero'){
      return this.formulario.value.datosUsuario.cargo;
    }
    return null;
  }
  registrarDatosUsuario(){
    console.log(this.usuario.id);
    this.startUpload(this.usuario.id);
    console.log('hola');
    this.snackBar.open('Se han actualizado los datos exitosamente', '200 OK', {
      duration: 3000,
    });
  }
}
