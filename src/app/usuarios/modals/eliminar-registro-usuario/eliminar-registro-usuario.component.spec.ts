import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { EliminarRegistroUsuarioComponent } from './eliminar-registro-usuario.component';

describe('EliminarRegistroUsuarioComponent', () => {
  let component: EliminarRegistroUsuarioComponent;
  let fixture: ComponentFixture<EliminarRegistroUsuarioComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ EliminarRegistroUsuarioComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(EliminarRegistroUsuarioComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
