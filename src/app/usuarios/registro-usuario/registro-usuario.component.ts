import { Component, OnInit, Input } from '@angular/core';
import { MatDialog } from '@angular/material';
import { Usuario } from '../../app.model';
import { EliminarRegistroUsuarioComponent } from '../../usuarios/modals/eliminar-registro-usuario/eliminar-registro-usuario.component';
import { BloqueoUsuarioComponent } from '../../usuarios/modals/bloqueo-usuario/bloqueo-usuario.component';
import Swal from 'sweetalert';

@Component({
  selector: 'app-registro-usuario',
  templateUrl: './registro-usuario.component.html',
  styleUrls: ['../../solicitud-interna/solicitud-interna.component.scss']
})
export class RegistroUsuarioComponent implements OnInit {

  @Input() usuario:Usuario;

  constructor(public dialog: MatDialog) { }

  ngOnInit() {
  }
  aceptarUsuarioDialog(){
    Swal('Solicitud aceptada', '¡Se ha registrado el usuario con éxito!', 'success');
    // const dialogRef = this.dialog.open(EliminarRegistroUsuarioComponent, {panelClass: 'test'});

    // dialogRef.afterClosed().subscribe(result => {
    //   console.log(`Dialog result: ${result} Closed`);
    // });
  }
  bloquearUsuarioDialog(){
    const dialogRef = this.dialog.open(BloqueoUsuarioComponent, {panelClass: 'test'});

    dialogRef.afterClosed().subscribe(result => {
      console.log(`Dialog result: ${result} Closed`);
    });
  }
  eliminarUsuarioDialog(){
    const dialogRef = this.dialog.open(EliminarRegistroUsuarioComponent, {panelClass: 'test'});

    dialogRef.afterClosed().subscribe(result => {
      console.log(`Dialog result: ${result} Closed`);
    });
  }
}
