import { EntryComponentModule } from './entry-component.module';

describe('EntryComponentModule', () => {
  let entryComponentModule: EntryComponentModule;

  beforeEach(() => {
    entryComponentModule = new EntryComponentModule();
  });

  it('should create an instance', () => {
    expect(entryComponentModule).toBeTruthy();
  });
});
