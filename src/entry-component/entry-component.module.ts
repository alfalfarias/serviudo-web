import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { BrowserModule } from '@angular/platform-browser';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

import { MaterialModule } from '../app/amaterial.module';

import { InteresadosListaComponent } from '../app/solicitud-interna/modals/interesados-lista/interesados-lista.component';
import { DetalleSisComponent } from '../app/solicitud-interna/sis/modals/detalle-sis/detalle-sis.component';
import { RegistroSisComponent } from '../app/solicitud-interna/sis/modals/registro-sis/registro-sis.component';
import { DetalleSisseComponent } from '../app/solicitud-interna/sisse/modals/detalle-sisse/detalle-sisse.component';
import { RegistroSisseComponent } from '../app/solicitud-interna/sisse/modals/registro-sisse/registro-sisse.component';
import { AprobacionSisComponent } from '../app/solicitud-interna/sis/modals/aprobacion-sis/aprobacion-sis.component';
import { AprobacionSisseComponent } from '../app/solicitud-interna/sisse/modals/aprobacion-sisse/aprobacion-sisse.component';
import { RechazarSolicitudComponent } from '../app/solicitud-interna/modals/rechazar-solicitud/rechazar-solicitud.component';
import { CargarResultadoSisseComponent } from '../app/solicitud-interna/sisse/modals/cargar-resultado-sisse/cargar-resultado-sisse.component';
import { CargarResultadoSisComponent } from '../app/solicitud-interna/sis/modals/cargar-resultado-sis/cargar-resultado-sis.component';
import { EliminarRegistroUsuarioComponent } from '../app/usuarios/modals/eliminar-registro-usuario/eliminar-registro-usuario.component';
import { BloqueoUsuarioComponent } from '../app/usuarios/modals/bloqueo-usuario/bloqueo-usuario.component';

@NgModule({
  entryComponents: [
    InteresadosListaComponent, 
    DetalleSisComponent, 
    RegistroSisComponent,
    DetalleSisseComponent,
    RegistroSisseComponent,
    AprobacionSisComponent,
    RechazarSolicitudComponent,
    CargarResultadoSisseComponent,
    CargarResultadoSisComponent,
    AprobacionSisseComponent,
    EliminarRegistroUsuarioComponent, 
    BloqueoUsuarioComponent
   ],
  imports: [
    CommonModule,
    MaterialModule,
    BrowserModule,
    FormsModule, 
    ReactiveFormsModule,
  ],
  declarations: [
    InteresadosListaComponent,
    DetalleSisComponent,
    RegistroSisComponent,
    DetalleSisseComponent,
    RegistroSisseComponent,
    AprobacionSisComponent,
    RechazarSolicitudComponent,
    CargarResultadoSisseComponent,
    CargarResultadoSisComponent,
    AprobacionSisseComponent,
    EliminarRegistroUsuarioComponent, 
    BloqueoUsuarioComponent
  ]
})
export class EntryComponentModule { }